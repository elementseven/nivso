<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pages
Route::get('/', [PageController::class, 'home'])->name('home');
Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/mental-health-and-peer-support', [PageController::class, 'mentalHealth'])->name('mental-health');
Route::get('/armed-forces-covenant', [PageController::class, 'armedForcesCovenant'])->name('armed-forces-covenant');
Route::get('/veterans-gateway', [PageController::class, 'veteransGateway'])->name('veterans-gateway');
Route::get('/research', [PageController::class, 'research'])->name('research');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');
Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');

// Services
Route::get('/support-organisations', [CharityController::class, 'charities'])->name('charities');
Route::get('/charities/get', [CharityController::class, 'get'])->name('charities.get');

// Commissioners Routes
Route::get('/veteran-champions', [CommissionerController::class, 'index'])->name('champions.index');
Route::get('/veteran-champions/{slug}', [CommissionerController::class, 'show'])->name('champions.show');

// Local Heroes Routes
Route::get('/local-heroes', [HeroController::class, 'index'])->name('heroes.index');
Route::get('/local-heroes/get', [HeroController::class, 'get'])->name('heroes.get');
Route::get('/local-heroes/{hero}/{slug}', [HeroController::class, 'show'])->name('heroes.show');

// News Routes
Route::get('/news', [NewsController::class, 'index'])->name('news.index');
Route::get('/news/get', [NewsController::class, 'get'])->name('news.get');
Route::get('/news/{date}/{slug}', [NewsController::class, 'show'])->name('news.show');
Route::get('/newsletters', [NewsController::class, 'newsletters'])->name('news.newsletters');
Route::get('/newsletters/get', [NewsController::class, 'getNewsletters'])->name('news.newsletters.get');

// Programmes Routes
Route::get('/programmes', [ProgrammeController::class, 'index'])->name('programmes.index');
Route::get('/programmes/get', [ProgrammeController::class, 'get'])->name('programmes.get');
Route::get('/programmes/{programme}/{slug}', [ProgrammeController::class, 'show'])->name('programmes.show');

// Events Routes
Route::get('/events', [EventController::class, 'index'])->name('events.index');
Route::get('/events/get', [EventController::class, 'get'])->name('events.get');
Route::get('/events/{date}/{slug}', [EventController::class, 'show'])->name('events.show');

// Send enquiry form
Route::post('/send-message', [SendMail::class, 'enquiry'])->name('send-message');
Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailing-list');

// Members Login Area
Route::get('/home', [HomeController::class, 'index'])->name('members-updates.index');
Route::get('/members-updates/get', [HomeController::class, 'get'])->name('members-updates.get');
Route::get('/members-updates/{date}/{slug}', [HomeController::class, 'show'])->name('members-updates.show');

Auth::routes();
Route::get('/logout', function () {
    Auth::logout();
    return redirect()->to('/login');
});