(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["MembersMenu"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMembersMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
pagetype:String},

mounted:function mounted(){
$(document).ready(function(){
var inview=new Waypoint({
element:$('#menu-trigger'),
handler:function handler(direction){
if(direction==="down"){
$('#main-menu').removeClass('top-on');
}
},
offset:'1%'});

var inview2=new Waypoint({
element:$('#menu-trigger-2'),
handler:function handler(direction){
if(direction==="up"){
$('#main-menu').addClass('top-on');
}
},
offset:'1%'});
// Initial state

var scrollPos=0;// adding scroll event

window.addEventListener('scroll',function(){
// detects new state and compares it with the new one
if(document.body.getBoundingClientRect().top>scrollPos){
$('#main-menu').addClass('on');
}else {
$('#main-menu').removeClass('on');
}// saves the new position for iteration.


scrollPos=document.body.getBoundingClientRect().top;
});
var lazyLoadInstance=new LazyLoad({
elements_selector:".lazy"});

$(".scroll-btn").click(function(){
$('html,body').animate({
scrollTop:$(".scrollTo").offset().top});

});
});
}};


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMembersMenuVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".menu-holder {\n  position: relative;\n  z-index: 100;\n}\n#menu-trigger {\n  position: fixed;\n  top: 200px;\n  left: 0;\n  width: 1px;\n  height: 1px;\n}\n#menu-trigger-2 {\n  position: fixed;\n  top: 100vh;\n  left: 0;\n  width: 1px;\n  height: 1px;\n}\n#main-menu-top {\n  z-index: 250;\n}\n#main-menu {\n  position: fixed;\n  top: -200px;\n  left: 0;\n  transition: all 300ms ease;\n}\n#main-menu.top-on {\n  top: 0 !important;\n}\n#main-menu.on {\n  top: 0;\n}\n.menu {\n  position: absolute;\n  left: 0;\n  z-index: 100;\n  width: 100%;\n  background-color: #fff;\n  box-shadow: 0rem 0rem 0.75rem rgba(0, 0, 0, 0.1) !important;\n}\n.menu .menu_logo {\n  width: 140px;\n  margin: 5px 0;\n}\n.menu .menu-links .menu-item {\n  display: inline-block;\n  padding-left: 2.5rem;\n  cursor: pointer;\n  font-family: \"PT Sans\", sans-serif;\n  font-weight: 700;\n  text-transform: none;\n  font-size: 1.1rem;\n  padding-top: 44px;\n  padding-bottom: 44px;\n  background-color: #fff;\n  position: relative;\n  z-index: 2;\n  transition: all 300ms ease;\n  color: #35318B;\n}\n.menu .menu-links .menu-item i {\n  font-size: 1.2rem;\n  margin-left: 10px;\n  color: #1C2D4B;\n}\n.menu .menu-links .menu-item i:hover {\n  color: #4D71A5;\n}\n.menu .menu-links .menu-item:hover, .menu .menu-links .menu-item.active {\n  color: #2E2B6F !important;\n}\n.menu .menu-top-links .menu-links .menu-item {\n  font-size: 0.8rem;\n  font-weight: 700;\n}\n.menu.opacity {\n  opacity: 0;\n}\n@media only screen and (max-width: 1230px) {\n.menu .menu-links .menu-item {\n    margin-left: 0rem;\n    font-size: 1rem;\n    padding-left: 2rem;\n}\n}\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n.menu .menu-links .menu-item {\n    margin-left: 0;\n}\n.menu .menu-links .menu-item:hover {\n    box-shadow: none;\n}\n#menu_btn {\n    display: block;\n}\n}",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMembersMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MembersMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/Menus/MembersMenu.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/Menus/MembersMenu.vue ***!
  \*******************************************************/
/***/function resourcesJsComponentsMenusMembersMenuVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _MembersMenu_vue_vue_type_template_id_244cc202___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./MembersMenu.vue?vue&type=template&id=244cc202& */"./resources/js/components/Menus/MembersMenu.vue?vue&type=template&id=244cc202&");
/* harmony import */var _MembersMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./MembersMenu.vue?vue&type=script&lang=js& */"./resources/js/components/Menus/MembersMenu.vue?vue&type=script&lang=js&");
/* harmony import */var _MembersMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./MembersMenu.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_MembersMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_MembersMenu_vue_vue_type_template_id_244cc202___WEBPACK_IMPORTED_MODULE_0__.render,
_MembersMenu_vue_vue_type_template_id_244cc202___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Menus/MembersMenu.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Menus/MembersMenu.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Menus/MembersMenu.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/***/function resourcesJsComponentsMenusMembersMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MembersMenu.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************/
/***/function resourcesJsComponentsMenusMembersMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MembersMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/Menus/MembersMenu.vue?vue&type=template&id=244cc202&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/Menus/MembersMenu.vue?vue&type=template&id=244cc202& ***!
  \**************************************************************************************/
/***/function resourcesJsComponentsMenusMembersMenuVueVueTypeTemplateId244cc202(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_template_id_244cc202___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_template_id_244cc202___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MembersMenu_vue_vue_type_template_id_244cc202___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MembersMenu.vue?vue&type=template&id=244cc202& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=template&id=244cc202&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=template&id=244cc202&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MembersMenu.vue?vue&type=template&id=244cc202& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMembersMenuVueVueTypeTemplateId244cc202(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",[
_c(
"div",
{
staticClass:"menu d-none d-lg-block top-on",
attrs:{id:"main-menu"}},

[
_c(
"div",
{
staticClass:
"container-fluid bg-primary px-5 py-1 d-none d-lg-block position-relative z-3"},

[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-lg-9"},[
_c("p",{staticClass:"mb-0 text-small"},[
_c(
"a",
{
staticClass:"text-white mr-4",
attrs:{href:"/veterans-gateway"}},

[_vm._v("Veterans Gateway")]),

_vm._v(" "),
_c(
"a",
{
staticClass:"text-white mr-4",
attrs:{href:"/armed-forces-covenant"}},

[_vm._v("Armed Forces Covenant")]),

_vm._v(" "),
_c(
"a",
{
staticClass:"text-white mr-4",
attrs:{href:"/faqs"}},

[_vm._v("FAQs")]),

_vm._v(" "),
_c(
"a",
{
staticClass:"text-white mr-4",
attrs:{href:"/training"}},

[_vm._v("Training")])])]),



_vm._v(" "),
_c("div",{staticClass:"col-lg-3 text-right"},[
_c("p",{staticClass:"text-small mb-0"},[
_c(
"a",
{
staticClass:"cursor-pointer text-white mr-5",
attrs:{href:"/login"}},

[
_c("i",{staticClass:"fa fa-user mr-2"}),
_vm._v("Login")]),


_vm._v(" "),
_c(
"a",
{
attrs:{
href:"https://www.facebook.com/NorthernIrelandVeteransSupportOffice",
"aria-label":"Facebook",
rel:"noreferrer",
target:"_blank"}},


[_c("i",{staticClass:"fa fa-facebook text-white"})]),

_vm._v(" "),
_c(
"a",
{
attrs:{
href:"https://www.instagram.com/ni_veterans_support_office/",
"aria-label":"Instagram",
rel:"noreferrer",
target:"_blank"}},


[
_c("i",{
staticClass:"fa fa-instagram text-white ml-4"})]),



_vm._v(" "),
_c(
"a",
{
attrs:{
href:"https://twitter.com/VeteransNi",
"aria-label":"Twitter",
rel:"noreferrer",
target:"_blank"}},


[
_c("i",{
staticClass:"fa fa-twitter text-white ml-4"})])])])])]),








_vm._v(" "),
_c(
"div",
{staticClass:"container-fluid px-5 mob-px-3 ipadp-px-3"},
[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-lg-3 py-3 page-home-pt-0"},[
_c("a",{attrs:{href:"/"}},[
_c("img",{
staticClass:"menu_logo mt-2 h-auto",
attrs:{
src:"/img/logos/logo-full.svg",
alt:"Northern Ireland Veteran's Support Office logo",
width:"140",
height:"72"}})])]),




_vm._v(" "),
_c(
"div",
{staticClass:"col-lg-9 text-right d-none d-lg-block"},
[
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/about"}},

[_vm._v("About")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/news"}},

[_vm._v("News")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{
href:"/local-heroes",
"aria-label":"Teamwear Ireland",
rel:"noreferrer"}},


[_vm._v("Local Heroes")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/support-commissioners"}},

[_vm._v("Support Commissioners")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/contact"}},

[_vm._v("Contact")])])])])])])]);










}];

_render._withStripped=true;



/***/}}]);

}());
