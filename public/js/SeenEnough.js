(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["SeenEnough"],{

/***/"./resources/js/components/SeenEnough.vue":
/*!************************************************!*\
  !*** ./resources/js/components/SeenEnough.vue ***!
  \************************************************/
/***/function resourcesJsComponentsSeenEnoughVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _SeenEnough_vue_vue_type_template_id_cbf96a14___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./SeenEnough.vue?vue&type=template&id=cbf96a14& */"./resources/js/components/SeenEnough.vue?vue&type=template&id=cbf96a14&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script={}


/* normalize component */;

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
script,
_SeenEnough_vue_vue_type_template_id_cbf96a14___WEBPACK_IMPORTED_MODULE_0__.render,
_SeenEnough_vue_vue_type_template_id_cbf96a14___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/SeenEnough.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/SeenEnough.vue?vue&type=template&id=cbf96a14&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/SeenEnough.vue?vue&type=template&id=cbf96a14& ***!
  \*******************************************************************************/
/***/function resourcesJsComponentsSeenEnoughVueVueTypeTemplateIdCbf96a14(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SeenEnough_vue_vue_type_template_id_cbf96a14___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SeenEnough_vue_vue_type_template_id_cbf96a14___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SeenEnough_vue_vue_type_template_id_cbf96a14___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SeenEnough.vue?vue&type=template&id=cbf96a14& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/SeenEnough.vue?vue&type=template&id=cbf96a14&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/SeenEnough.vue?vue&type=template&id=cbf96a14&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/SeenEnough.vue?vue&type=template&id=cbf96a14& ***!
  \**********************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsSeenEnoughVueVueTypeTemplateIdCbf96a14(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"text-center text-lg-left"},[
_c("p",{staticClass:"mimic-h2"},[_vm._v("Thinking of joining us?")]),
_vm._v(" "),
_c("p",[
_vm._v(
"New players are always welcome at our training sessions on Tuesday and Thursday evenings at Belvoir and Ashfield School. Get in touch and let us know you're coming!")]),


_vm._v(" "),
_c("a",{attrs:{href:"/join"}},[
_c(
"button",
{staticClass:"btn btn-primary",attrs:{type:"button"}},
[_vm._v("Join")])])]);



}];

_render._withStripped=true;



/***/}}]);

}());
