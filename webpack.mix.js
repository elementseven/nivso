let mix = require('laravel-mix');
require('laravel-mix-purgecss');
const TargetsPlugin = require('targets-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
  postCss: [
    require('autoprefixer'),
  ],
});

mix.babelConfig({
  plugins: ['@babel/plugin-syntax-dynamic-import']
});

mix.webpackConfig({
    plugins: [
    new TargetsPlugin({
      browsers: ['last 2 versions', 'chrome >= 41', 'IE 11'],
    }),
  ],
  output: {
    chunkFilename: 'js/[name].js?id=[chunkhash]',
  },
});

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .extract()
    .sass('resources/sass/app.scss', 'public/css')
    .purgeCss()
    .minify('public/css/app.css')
    .version();