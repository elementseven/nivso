<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouncilOrganisationPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('council_organisation', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->index();
            $table->foreign('council_id')->references('id')->on('councils')->onDelete('cascade');
            $table->bigInteger('organisation_id')->unsigned()->index();
            $table->foreign('organisation_id')->references('id')->on('organisations')->onDelete('cascade');
            $table->primary(['council_id', 'organisation_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('council_organisation');
    }
}
