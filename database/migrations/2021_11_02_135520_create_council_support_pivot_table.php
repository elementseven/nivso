<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouncilSupportPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('council_support', function (Blueprint $table) {
            $table->bigInteger('council_id')->unsigned()->index();
            $table->foreign('council_id')->references('id')->on('councils')->onDelete('cascade');
            $table->bigInteger('support_id')->unsigned()->index();
            $table->foreign('support_id')->references('id')->on('supports')->onDelete('cascade');
            $table->primary(['council_id', 'support_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('council_support');
    }
}
