<?php

namespace App\Nova;

use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Froala\NovaFroalaField\Froala;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Commissioner extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Commissioner::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'last_name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'first_name',
        'last_name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('commissioners');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('commissioners', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('commissioners', 'thumbnail');
            })->deletable(false),
            Text::make('Name', 'full_name')->exceptOnForms()->sortable(),
            Text::make('Title')->hideFromIndex(),
            Text::make('First Name')->hideFromIndex(),
            Text::make('Last Name')->hideFromIndex(),
            Text::make('Phone'),
            Text::make('Email'),
            Text::make('Facebook')->nullable()->hideFromIndex(),
            Text::make('Instagram')->nullable()->hideFromIndex(),
            Text::make('Linkedin')->nullable()->hideFromIndex(),
            Froala::make('Description')->withFiles('public'),
            BelongsTo::make('Council')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
