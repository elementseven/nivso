<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Froala\NovaFroalaField\Froala;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Programme extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Programme::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            Text::make('Excerpt')->sortable()->onlyOnForms(),
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('programmes');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('programmes', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('programmes', 'thumbnail');
            })->deletable(false)->required(),
            Select::make('Status')->options($statuses)->sortable(),
            Froala::make('Content')->withFiles('public'),
            Textarea::make('Meta Description','meta_description')->onlyOnForms(),
            Textarea::make('Keywords','keywords')->onlyOnForms(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
