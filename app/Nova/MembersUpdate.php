<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Froala\NovaFroalaField\Froala;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class MembersUpdate extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MembersUpdate::class;
    public static $with = ['media'];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Published for Individuals'=>'Published for Individuals','Draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            Date::make('Date', 'created_at')->onlyOnIndex(),
            Text::make('Excerpt')->sortable()->onlyOnForms(),
            // Image::make('Photo')->store(function (Request $request, $model) {
            //         $model->addMediaFromRequest('photo')->toMediaCollection('news');
            //     })
            //     ->preview(function () {
            //         return $this->getFirstMediaUrl('news', 'thumbnail');
            //     })
            //     ->thumbnail(function () {
            //         return $this->getFirstMediaUrl('news', 'thumbnail');
            // })->deletable(false),
            Select::make('Status')->options($statuses)->sortable(),
            Froala::make('Content')->withFiles('public'),
            HasMany::make('Documents', 'documents'),
            BelongsToMany::make('Users')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
