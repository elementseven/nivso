<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Newsletter;
use App\Models\Category;
use App\Models\Post;

class NewsController extends Controller
{

  public function newsletters(){
    return view('newsletters.index');
  }

  public function getNewsletters(Request $request){

    $newsletters = Newsletter::where('status','!=','draft')
    ->orderBy('created_at','desc')
    ->paginate($request->input('limit'),['id','title', 'slug', 'created_at']);
  
    foreach($newsletters as $nl){

      $nl->featured = $nl->getFirstMediaUrl('newsletter-images', 'normal');
      $nl->featuredwebp = $nl->getFirstMediaUrl('newsletter-images', 'normal-webp');
      $nl->mimetype = $nl->getFirstMedia('newsletter-images')->mime_type;

      $nl->link = $nl->getFirstMediaUrl('newsletters');
      $nl->size = $this->formatBytes($nl->getFirstMedia('newsletters')->size);
    }
    return $newsletters;

  }

    // Main news page
  public function index(){
    $categories = Category::has('posts')->orderBy('name','asc')->get();
    return view('news.index')->with(['categories' => $categories]);
  }

  // Return json news posts
  public function get(Request $request){

    $search = $request->input('search');

    if($request->input('category') == '*'){
      $posts = Post::where('status','!=','draft')
      ->when($search != "", function($q) use ($search) {
        $q->where('title','LIKE', '%'.$search.'%');
      })
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }else{
      $posts = Post::orderBy('created_at','desc')->whereHas('categories', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->when($search != "", function($q) use ($search) {
        $q->where('title','LIKE', '%'.$search.'%');
      })
      ->where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }

    foreach($posts as $p){
      $p->normal = $p->getFirstMediaUrl('news', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('news', 'normal-webp');
      $p->double = $p->getFirstMediaUrl('news', 'double');
      $p->doublewebp = $p->getFirstMediaUrl('news', 'double-webp');
      $p->featured = $p->getFirstMediaUrl('news', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('news', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('news')->mime_type;
    }

    return $posts;

  }

  // Single news article
  public function show($date, $slug){

    $post = Post::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
    $others = Post::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(2);
    return view('news.show')->with(['post' => $post, 'others' => $others]);

  }

  private function formatBytes($size, $precision = 2)
  {
    $base = log($size, 1024);
    $suffixes = array('', 'kb', 'mb', 'gb', 'tb');   

    return round(pow(1024, $base - floor($base)), $precision) .''. $suffixes[floor($base)];
  }

}
