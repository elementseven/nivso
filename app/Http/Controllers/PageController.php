<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Research;
use App\Models\Event;
use App\Models\Post;
use App\Models\Hero;
use Carbon\Carbon;

class PageController extends Controller
{
    public function home(){
        $event = Event::where('status','published')->whereDate('date','>=',Carbon::now())->orderBy('date', 'asc')->first();
        if($event){
            $event->webp = $event->getFirstMediaUrl('events','normal-webp');
            $event->image = $event->getFirstMediaUrl('events','normal');
            $event->mime = $event->getFirstMedia('events')->mime_type;
        }
        $featured = Post::where('status','Featured')->orderBy('created_at')->paginate(3);
        $hero = Hero::where('featured', true)->first();
        $hero->webp = $hero->getFirstMediaUrl('heroes','normal-webp');
        $hero->image = $hero->getFirstMediaUrl('heroes','normal');
        $hero->mime = $hero->getFirstMedia('heroes')->mime_type;
        return view('home')->with(['featured' => $featured, 'hero' => $hero, 'event' => $event]);
    }

    public function about(){
        return view('about');
    }
    public function mentalHealth(){
        return view('mentalHealth');
    }
    public function research(){
        $northernireland = Research::where('category', 'Northern Ireland')->get();
        foreach($northernireland as $research){
            $research->webp = $research->getFirstMediaUrl('research-images','normal-webp');
            $research->image = $research->getFirstMediaUrl('research-images','normal');
            $research->mime = $research->getFirstMedia('research-images')->mime_type;
        }
        $unitedkingdom = Research::where('category', 'United Kingdom')->get();
        foreach($unitedkingdom as $research){
            $research->webp = $research->getFirstMediaUrl('research-images','normal-webp');
            $research->image = $research->getFirstMediaUrl('research-images','normal');
            $research->mime = $research->getFirstMedia('research-images')->mime_type;
        }
        return view('research')->with(['northernireland' => $northernireland, 'unitedkingdom' => $unitedkingdom]);
    }
    public function contact(){
        return view('contact');
    }
    public function veteransGateway(){
        return view('veterans-gateway');
    }
    public function armedForcesCovenant(){
        return view('armed-forces-covenant');
    }
    public function tandcs(){
        return view('tandcs');
    }
    public function privacyPolicy(){
        return view('privacyPolicy');
    }
    
}
