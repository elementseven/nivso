<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MembersUpdate;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('members-updates.dashboard');
    }


    // Return json news posts
    public function get(Request $request){

        $user = Auth::user();
        $posts = MembersUpdate::whereHas('users', function($q) use($user){
              $q->where('id', $user->id);
        })
        ->orWhere('status','Published')
        ->orderBy('created_at','desc')
        ->with('documents')
        ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);

        foreach($posts as $post){
            foreach($post->documents as $doc){
                $doc->link = $doc->getFirstMediaUrl('documents');
            }
        }

        return $posts;
    }

    // Single news article
    public function show($date, $slug){
        $post = MembersUpdate::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
        $others = MembersUpdate::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(2);
        return view('members-updates.show')->with(['post' => $post, 'others' => $others]);
    }


}
