<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Support;
use App\Models\Council;

class CharityController extends Controller
{

    public function charities(){
        $councils = Council::has('supports')->orderBy('name','asc')->get();
        return view('charities')->with(['councils' => $councils]);
    }


    public function get(Request $request){
        $search = $request->input('search');
        if($request->input('council') == '*'){
          $supports = Support::when($search != "", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
          })
          ->orderBy('name','desc')
          ->with('councils')
          ->paginate($request->input('limit'));
        }else{
          $supports = Support::whereHas('councils', function($q) use($request){
              $q->where('id', $request->input('council'));
          })
          ->when($search != "", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
          })
          ->orderBy('name','desc')
          ->with('councils')
          ->paginate($request->input('limit'));
        }

        foreach($supports as $p){
          $p->logo = $p->getFirstMediaUrl('charities', 'normal');
          $p->logowebp = $p->getFirstMediaUrl('charities', 'normal-webp');
          $p->mimetype = $p->getFirstMedia('charities')->mime_type;
        }

        return $supports;
    }
}
