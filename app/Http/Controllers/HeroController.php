<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hero;

class HeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('heroes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $search = $request->input('search');
        $heros = Hero::orderBy('created_at','desc')
        ->when($search != "", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
        })
        ->paginate($request->input('limit'),['id','name', 'slug', 'excerpt','created_at']);

        foreach($heros as $hero){
          $hero->normal = $hero->getFirstMediaUrl('heroes', 'normal');
          $hero->normalwebp = $hero->getFirstMediaUrl('heroes', 'normal-webp');
          $hero->double = $hero->getFirstMediaUrl('heroes', 'double');
          $hero->doublewebp = $hero->getFirstMediaUrl('heroes', 'double-webp');
          $hero->mob = $hero->getFirstMediaUrl('heroes', 'mob');
          $hero->mobwebp = $hero->getFirstMediaUrl('heroes', 'mob-webp');
          $hero->mimetype = $hero->getFirstMedia('heroes')->mime_type;
        }

        return $heros;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hero $hero)
    {
        $others = Hero::where('id','!=', $hero->id)->orderBy('created_at','desc')->paginate(1);
        return view('heroes.show')->with(['hero' => $hero, 'others' => $others]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
