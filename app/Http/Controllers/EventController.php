<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Carbon\Carbon;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('events.index');
    }

    /**
     * Get an array of events.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){

        $search = $request->input('search');

        $events = Event::where('status','!=','draft')
        ->when($search != "", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
        })
        ->whereDate('date', '>=', Carbon::now())
        ->orderBy('date','asc')
        ->paginate($request->input('limit'),['id','name', 'status', 'location', 'slug', 'excerpt','date']);

        foreach($events as $e){
            $e->normal = $e->getFirstMediaUrl('events', 'normal');
            $e->normalwebp = $e->getFirstMediaUrl('events', 'normal-webp');
            $e->double = $e->getFirstMediaUrl('events', 'double');
            $e->doublewebp = $e->getFirstMediaUrl('events', 'double-webp');
            $e->featured = $e->getFirstMediaUrl('events', 'featured');
            $e->featuredwebp = $e->getFirstMediaUrl('events', 'featured-webp');
            $e->mimetype = $e->getFirstMedia('events')->mime_type;
        }

      return $events;

  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date, $slug){
        $event = Event::where([['slug',$slug],['status','!=','draft']])->whereDate('date', $date)->first();
        $others = Event::where('id','!=', $event->id)->where('status','!=','draft')->whereDate('date', '>=', Carbon::now())->orderBy('date','desc')->paginate(2);
        return view('events.show')->with(['event' => $event, 'others' => $others]);
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
