<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

use Newsletter;
use Mail;
use Auth;


class SendMail extends Controller
{
  public function enquiry(Request $request){

    // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "General Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        Mail::send('emails.enquiry',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $phone){
            $message->from('donotreply@nivso.org.uk', 'NIVSO');
            $message->subject($subject);
            $message->replyTo($email);
            // $message->to('ni-vsocomms@rfca.mod.uk');
            $message->to('ni-vso@rfca.mod.uk');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }


    public function mailingListSignup(Request $request){
        $this->validate($request,[
          'email' => 'required|string|email|max:255'
        ]);
        Newsletter::subscribe($request->input('email'));
        return "success";
    }
}