<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Programme;


class ProgrammeController extends Controller
{
  // Main programmes page
  public function index(){
    return view('programmes.index');
  }

  // Return json programmes
  public function get(Request $request){

    $search = $request->input('search');

    $programmes = Programme::where('status','!=','draft')
    ->when($search != "", function($q) use ($search) {
      $q->where('title','LIKE', '%'.$search.'%');
    })
    ->orderBy('created_at','desc')
    ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);

    foreach($programmes as $p){
      $p->normal = $p->getFirstMediaUrl('programmes', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('programmes', 'normal-webp');
      $p->double = $p->getFirstMediaUrl('programmes', 'double');
      $p->doublewebp = $p->getFirstMediaUrl('programmes', 'double-webp');
      $p->featured = $p->getFirstMediaUrl('programmes', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('programmes', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('programmes')->mime_type;
    }

    return $programmes;

  }

  // Single programmes
  public function show(Programme $programme, $slug){

    $others = Programme::where('id','!=', $programme->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(2);
    return view('programmes.show')->with(['programme' => $programme, 'others' => $others]);

  }
}
