<?php

namespace App\Policies;

use App\Models\Faq;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FaqPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create a post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Faq  $post
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name == 'Admin';
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Faq  $upfaq
     * @return mixed
     */
    public function update(User $user, Faq $upfaq)
    {
        return $user->role->name == 'Admin';
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Faq  $viewfaq
     * @return mixed
     */
    public function view(User $user, Faq $viewfaq)
    {
        return $user->role->name == 'Admin';
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Faq  $delfaq
     * @return mixed
     */
    public function delete(User $user, Faq $delfaq)
    {
        return $user->role->name == 'Admin';
    }

    /**
     * Determine whether the user can view faqs.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Faq  $faq
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->name == 'Admin';
    }
}
