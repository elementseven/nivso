<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Document extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name','slug','file','members_update_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($document) {
            $document->slug = Str::slug($document->name, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('documents')->singleFile();
    }

    public function membersUpdates(){
        return $this->belongsTo('App\Models\MembersUpdate','members_update_id');
    }
}
