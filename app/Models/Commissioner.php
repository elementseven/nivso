<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Commissioner extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'title',
      'first_name',
      'last_name',
      'full_name',
      'photo',
      'description',
      'phone',
      'email',
      'facebook',
      'instagram',
      'linkedin',
      'council_id'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($commissioner) {
            $commissioner->full_name = $commissioner->first_name . ' ' . $commissioner->last_name;
        });
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(443);
        $this->addMediaConversion('normal-webp')->width(443)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(886);
        $this->addMediaConversion('double-webp')->width(886)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('commissioners')->singleFile();
    }

    public function council(){
        return $this->belongsTo('App\Models\Council');
    }
}
