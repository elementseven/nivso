<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Organisation extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'name',
      'website',
      'image',
      'phone',
      'description',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(443);
        $this->addMediaConversion('normal-webp')->width(443)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(886);
        $this->addMediaConversion('double-webp')->width(886)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('organisations')->singleFile();
    }

    public function councils(){
        return $this->belongsToMany('App\Models\Council', 'council_organisation')->withPivot('council_id');
    }
}
