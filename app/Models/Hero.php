<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Hero extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'name',
      'slug',
      'image',
      'featured',
      'excerpt',
      'description'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($hero) {
            $hero->slug = Str::slug($hero->name, "-");
        });
    }
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('mob')->keepOriginalImageFormat()->width(300);
        $this->addMediaConversion('mob-webp')->width(300)->format('webp');
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(536);
        $this->addMediaConversion('normal-webp')->width(536)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(1072);
        $this->addMediaConversion('double-webp')->width(1072)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('heroes')->singleFile();
    }

}
