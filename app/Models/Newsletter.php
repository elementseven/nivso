<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Newsletter extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    
    protected $fillable = [
      'title',
      'slug',
      'status',
      'file',
      'image'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($newsletter) {
            $newsletter->slug = Str::slug($newsletter->title, "-");
        });
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(300);
        $this->addMediaConversion('normal-webp')->width(300)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
    }

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('newsletter-images')->singleFile();
        $this->addMediaCollection('newsletters')->singleFile();
    }
}
