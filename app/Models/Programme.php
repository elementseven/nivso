<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Programme extends  Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title',
      'excerpt',
      'meta_description',
      'keywords',
      'content',
      'photo',
  ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($programme) {
            $programme->slug = Str::slug($programme->title, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(800);
        $this->addMediaConversion('normal-webp')->width(800)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(1600);
        $this->addMediaConversion('double-webp')->width(1600)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
        $this->addMediaConversion('featured')->keepOriginalImageFormat()->crop('crop-center', 350, 211);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 350, 211)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('programmes')->singleFile();
    }

}
