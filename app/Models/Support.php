<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Support extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'name',
      'website',
      'phone',
      'image'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(410);
        $this->addMediaConversion('normal-webp')->width(410)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 300, 300);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('charities')->singleFile();
    }

    public function councils(){
        return $this->belongsToMany('App\Models\Council', 'council_support')->withPivot('council_id');
    }

}
