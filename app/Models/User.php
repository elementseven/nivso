<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'first_name', 'last_name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $attributes = [
        'role_id' => 2,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($user) {
            $user->full_name = $user->first_name . ' ' . $user->last_name;
        });
    }

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }

    public function profile(){
        return $this->hasOne('App\Models\Profile');
    }

    public function membersUpdates(){
        return $this->belongsToMany('App\Models\MembersUpdate', 'members_updates_users')->withPivot('members_update_id');
    }

}