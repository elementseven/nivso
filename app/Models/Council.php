<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Council extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'name',
      'slug',
      'logo'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($council) {
            $council->slug = Str::slug($council->name, "-");
        });
    }
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->height(77);
        $this->addMediaConversion('normal-webp')->height(77)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->height(154);
        $this->addMediaConversion('double-webp')->height(154)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 154, 154);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('councils')->singleFile();
    }

    public function commissioner(){
        return $this->hasOne('App\Models\Commissioner');
    }

    public function organisations(){
        return $this->belongsToMany('App\Models\Organisation', 'council_organisation')->withPivot('organisation_id');
    }
    public function supports(){
        return $this->belongsToMany('App\Models\Support', 'council_support')->withPivot('support_id');
    }
}
