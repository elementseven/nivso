<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
  use HasFactory;

  protected $fillable = [
      'name','slug'
  ];
  protected static function boot()
  {
      parent::boot();
      static::saving(function ($faqcategory) {
          $slug = strtolower($faqcategory->name);
          //Make alphanumeric (removes all other characters)
          $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
          //Clean up multiple dashes or whitespaces
          $slug = preg_replace("/[\s-]+/", " ", $slug);
          //Convert whitespaces and underscore to dash
          $slug = preg_replace("/[\s_]/", "-", $slug);

          $faqcategory->slug = $slug;
      });
  }

  public function faqs(){
      return $this->hasMany('App\Models\Faq');
  }
}
