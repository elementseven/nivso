@php
$page = 'Terms & Conditions';
$pagetitle = "Terms & Conditions - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "NIVSO Terms & Conditions";
$pagetype = 'dark';
$pagename = 'tandcs';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5">
	<div class="row">
		<div class="container mb-4 mob-px-4 text-center text-lg-left">
			<h1 class="mb-4 pt-5">Terms & Conditions</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
@endsection