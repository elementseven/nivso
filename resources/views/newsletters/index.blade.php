@php
$page = 'Homepage';
$pagetitle = "Newsletters - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua.";
$pagetype = 'dark';
$pagename = 'newsletters';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding mt-5 pb-4">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex d-lg-none" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row pt-5">
		<div class="container text-center text-lg-left">
			<h1 class="">Newsletters</h1>
			<p>Click on a newsletter below to download a pdf.</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<newsletters-index></newsletters-index>
@endsection