@php
$page = 'Veterans Gateway';
$pagetitle = "Veterans' Gateway - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "The Veterans’ Gateway aims to make things easier by putting Veterans and their families in touch with the organisations best placed to help with the information, advice and support they need.";
$pagetype = 'dark';
$pagename = 'veterans-gateway';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding mt-5">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row">
		<div class="container mb-4 mob-px-4 text-center text-lg-left">
			<h1 class="mb-4  pt-5">Veterans' Gateway</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 mob-px-4 text-center text-lg-left z-2 position-relative">
	<div class="row">
		<div class="col-12">
			<p>There is an extensive network of organisations supporting the Armed Forces community, so finding the right one for your needs can be frustrating. The Veterans’ Gateway aims to make things easier by putting Veterans and their families in touch with the organisations best placed to help with the information, advice and support they need. A wide range of support topics are covered, from healthcare and housing to employability, finances, personal relationships and more. This means that whatever support you need, the Veterans’ Gateway can be your first point of contact. </p>
			<p>The Veterans’ Gateway website contains a wealth of information on the different types of support available, and a team of expert advisers is available 24/7 to talk to by phone, email, text, or live chat. Many members of the Veterans’ Gateway team are Veterans themselves, so they understand the issues that people face after leaving the Armed Forces.</p>
			<a href="https://www.veteransgateway.org.uk/" target="_blank">
				<button class="btn btn-primary">Visit Veterans Gateway</button>
			</a>
		</div>
		<div class="col-12 py-5">
			<p class="mimic-h3 mt-5">Directory of Services</p>
			<p>One of the helpful resources on the Veterans' Gateway website is the map of local support organisations, also called the Directory of Services. We have made a short video to help you navigate the map and get a better picture of the services available in your local area.</p>
			<p>The Directory is growing rapidly, and new organisations offering support are being added to it all the time. The VSO is developing the directory to ensure that it contains Northern Ireland-specific information.  Searches of the Directory will help us learn more about the type of support needed in specific areas. All of this information can be used to justify the creation of new Veteran services and the enhancement of existing ones.</p>
			<p>So, the more the Directory is used, the greater the difference it can make. By using the map you are playing a part in bringing about positive changes for Veterans in your community by highlighting gaps in current service provision and the need for new local services.</p>
		</div>

		<div class="col-lg-4 mt-5 mob-mt-0">
			<p class="mimic-h3 mt-5">How to use the Directory of Services</p>
			<p>In order to get the most from the Veterans Gateway local services Directory, we suggest you watch our 30 second video then Click on the button below.</p>
			<a href="https://www.veteransgateway.org.uk/" target="_blank">
				<button class="btn btn-primary">Visit Veterans Gateway</button>
			</a>
		</div>
		<div class="col-lg-8 mt-5">
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-4OjGgWLoxY?rel=0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<div class="row pt-5">
		<div class="col-lg-6 pt-5">
			<picture>
        <source src="/img/misc/veterans-gateway.webp" type="image/webp"/>
        <source src="/img/misc/veterans-gateway.jpg" type="image/jpeg"/>
        <img src="/img/misc/veterans-gateway.jpg" alt="Veterans Gateway App" class="w-100 h-auto d-none d-lg-block" width="1000" height="1113" />
      </picture>
    </div>
    <div class="col-lg-6 mt-5 mob-mt-0">
			<p class="mimic-h3 mt-5 mob-mt-0">The Veterans’ Gateway app</p>
			<p>The app, offering access to the Directory of Local Support, also has links to allow an individual to call, email or live chat with the team at the contact centre – that continues to be fully staffed 24/7 by Connect Assist advisers.</p>
			<p class="mb-4">The app is now available for free on the <a href="https://apps.apple.com/us/app/veterans-gateway-directory-uk/id1488014248" target="_blank"><u>Apple App Store</u></a> and <a href="https://play.google.com/store/apps/details?id=com.rview_mobile&hl=en_us" target="_blank"><u>Google Play</u></a>. There is also a <a href="https://www.youtube.com/watch?v=if9kT7UgWT4" target="_blank"><u>YouTube video</u></a> of the app in use.</p>
			<a href="https://apps.apple.com/us/app/veterans-gateway-directory-uk/id1488014248">
				<picture>
	        <source src="/img/misc/app-store.webp" type="image/webp"/>
	        <source src="/img/misc/app-store.jpg" type="image/jpeg"/>
	        <img src="/img/misc/app-store.jpg" alt="Veterans Gateway App on Apple App Store" class="w-100 h-auto" width="300" height="88" style="max-width: 200px;cursor: pointer;" />
	      </picture>
	    </a>
	    <a href="https://play.google.com/store/apps/details?id=com.rview_mobile&hl=en_us">
				<picture>
	        <source src="/img/misc/play-store.webp" type="image/webp"/>
	        <source src="/img/misc/play-store.jpg" type="image/jpeg"/>
	        <img src="/img/misc/play-store.jpg" alt="Veterans Gateway App on Google Play Store" class="w-100 h-auto ml-2 mob-ml-0" width="300" height="88"  style="max-width: 200px;cursor: pointer;"/>
	      </picture>
	    </a>
		</div>
	</div>
</div>
@endsection