@php
$page = 'Homepage';
$pagetitle = "Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "We are the lead support and signposting service linking Veterans and charities supporting Veterans in Northern Ireland.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])

@section('styles')
@if($event)
<style>
    .no-webp .event-img{
        background-image: url('{{$event->image}}');
    }
    .webp .event-img{
        background-image: url('{{$event->webp}}');
    }
</style>
@endif
<link rel="preload" as="image" href="/img/misc/ni.png">
<link rel="preload" as="image" href="/img/misc/ni.webp">
@endsection
@section('header')
<header id="home-header" class="container position-relative z-3 top-padding">
    <div class="row justify-content-center mob-py-5">
        
        <div class="col-lg-8 position-relative home-header-min-height mob-py-5">
            <picture>
                <source data-src="/img/misc/ni.webp" type="image/webp"/>
                <source data-src="/img/misc/ni.png" type="image/png"/>
                <img data-src="/img/misc/ni.png" alt="NIVSO - Northern Ireland Background" class="home-header-ni lazy h-auto" width="775" height="596" />
            </picture>
            <div class="d-md-table w-100 h-100 px-4 mob-px-0 position-relative z-2">
                <div class="d-md-table-cell align-middle w-100 h-100 text-center text-lg-left">
                    <h1 class="mb-3">Northern Ireland <br/>Veterans’<br class="d-lg-none" />Support Office</h1>
                    <p class="mb-4 pr-5 mob-px-4">We are the lead support and signposting service linking Veterans and charities supporting Veterans in Northern Ireland.</p>
                    <a href="/support-organisations">
                        <button class="btn btn-primary">Find Support</button>
                    </a>
                </div>
            </div>
        </div>
{{--         <div class="col-10 position-relative d-lg-none">
            <picture>
                <source src="/img/home/brit-vet.webp" type="image/webp"/>
                <source src="/img/home/brit-vet.png" type="image/png"/>
                <img src="/img/home/brit-vet.png" alt="NIVSO - British Soilder Holding a Helmet" class="w-100" />
            </picture>
        </div> --}}
        <div class="col-lg-4 position-relative d-none d-lg-block">
            <picture>
                <source data-src="/img/home/brit-vet.webp" type="image/webp"/>
                <source data-src="/img/home/brit-vet.png" type="image/png"/>
                <img data-src="/img/home/brit-vet.png" alt="NIVSO - British Soilder Holding a Helmet" class="home-header-soilder lazy" />
            </picture>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative mob-mb-0 z-2 bg-primary text-white">
    <div class="row">
        <div class="container py-5">
            <div class="row my-5 mob-my-3">
                <div class="col-12 text-center text-lg-left">
                    <p class="mimic-h2 mb-5">How can we help?</p>
                </div>
                <div class="col-lg-6 text-center text-lg-left mb-4">
                    <div class="row">
                        <div class="col-lg-2 mob-mb-3 ipadp-mb-3">
                            <img src="/img/icons/hands.svg" class="NIVSO - Helping Hands icon" class="w-100 h-auto" width="60" height="40"/>
                        </div>
                        <div class="col-lg-10 pr-4 mob-px-3">
                            <p>We build the capacity to deliver the Armed Forces Covenant - the Nation’s commitment to Veterans. It is a pledge that ensures Veterans and their families have access to a wide variety of support and are treated fairly.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-center text-lg-left mb-4">
                    <div class="row">
                        <div class="col-lg-2 mob-mb-3 ipadp-mb-3">
                            <img src="/img/icons/education.svg" class="NIVSO - Education icon" class="w-100 h-auto" width="60" height="40"/>
                        </div>
                        <div class="col-lg-10 pr-4 mob-px-3">
                            <p>We educate key players within local communities regarding the needs of Veterans, offering solutions to problems, suggestions and ideas, advocacy, signposting and practical help.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-center text-lg-left mb-4">
                    <div class="row">
                        <div class="col-lg-2 mob-mb-3 ipadp-mb-3">
                            <img src="/img/icons/hands-people.svg" class="NIVSO - Helping icon" class="w-100 h-auto" width="60" height="60"/>
                        </div>
                        <div class="col-lg-10 pr-4 mob-px-3">
                            <p>We work collaboratively with the Office of the Veterans’ Commissioner for Northern Ireland, and support individual Veterans, their families and carers, as well as groups and organisations that provide a range of support services and activities for Veterans.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 text-center text-lg-left">
                    <div class="row">
                        <div class="col-lg-2 mob-mb-3 ipadp-mb-3">
                            <img src="/img/icons/network.svg" class="NIVSO - Helping network icon" class="w-100 h-auto" width="60" height="60"/>
                        </div>
                        <div class="col-lg-10 pr-4 mob-px-3">
                            <p>We advocate a holistic approach to providing options to Veterans and help them to make informed decisions. We sit at the heart of the network, where we interact top down and bottom up, and endeavour to find a solution to all Veterans’ diverse needs.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid position-relative mob-mb-0 z-2 bg-primary-half">
    <div class="row">
        <div class="container">

            <div class="embed-responsive embed-responsive-16by9 shadow">
              <iframe class="embed-responsive-item lazy" data-src="https://www.youtube.com/embed/h35tcYwzS4g?rel=0" allowfullscreen></iframe>
            </div>

        </div>
    </div>
</div>
<div class="container py-5">
    <div class="row py-5">
        <div class="col-lg-6 text-center">
            <p class="text-small text-primary letter-spacing text-uppercase mb-0 d-lg-none"><b>Featured local hero story</b></p>
            <p class="mimic-h2 d-lg-none">{{$hero->name}}</p>
            <picture>
                <source data-src="{{$hero->webp}}" type="image/webp"/>
                <source data-src="{{$hero->image}}" type="{{$hero->mime}}"/>
                <img data-src="{{$hero->image}}" alt="{{$hero->name}} - Local Hero Story NIVSO" class="w-100 h-auto lazy" width="536" height="403" />
            </picture>
        </div>
        <div class="col-lg-6 pl-5 mob-px-3 mob-pt-3 ipadp-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
                    <p class="text-small text-primary letter-spacing text-uppercase mb-0 d-none d-lg-block"><b>Featured local hero story</b></p>
                    <p class="mimic-h2 d-none d-lg-block">{{$hero->name}}</p>
                    <p class="">{{$hero->excerpt}}</p>
                    <a href="/local-heroes/{{$hero->id}}/{{$hero->slug}}">
                        <button type="button" class="btn btn-primary">Read Full Story</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5 py-5 mob-py-0">
        <div class="col-lg-6 text-center text-lg-left">
            <p class="text-larger text-statement-mob"><b>There are many other heroes who have shared their inspirational stories with us.</b></p>
            <a href="/local-heroes">
                <button type="button" class="btn btn-primary">View all stories</button>
            </a>
        </div>
    </div>
</div>
<div class="container-fluid position-relative mob-mb-0 z-2 bg-primary">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 my-5 py-5 text-center text-lg-left">
                    <p class="text-larger text-white text-statement-mob"><b>Can we help you? We will help find the type of support that’s right for you.</b></p>
                    <a href="/contact">
                        <button type="button" class="btn btn-white">Get in touch</button>
                    </a>
                </div>
                <div class="col-lg-6 d-none d-lg-block">
                    <picture>
                        <source data-src="/img/home/hex-1.webp" type="image/webp"/>
                        <source data-src="/img/home/hex-1.png" type="image/png"/>
                        <img data-src="/img/home/hex-1.png" alt="NIVSO - heroes hexagons" class="w-100 h-auto home-hex-1 lazy" width="619" height="612" />
                    </picture>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid py-5 mob-pb-3">
    @if($event != null)
    <div class="row py-5">
        <div class="container">
            <p class="mimic-h2 mb-2 text-center mb-4 d-lg-none">Upcoming Events</p>
            <div class="row">
                <div class="col-lg-7">
                    <div class="event-img bg"></div>
                </div>
                <div class="col-lg-5">
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell align-middle w-100 h-100">
                            <div class="card home-event-box p-5 mob-px-3 text-center text-lg-left">
                                <p class="text-small text-primary letter-spacing text-uppercase mb-0"><b>Upcoming Events</b></p>
                                <p class="mimic-h2">{{$event->name}}</p>
                                <div class="line"><span></span></div>
                                <p class="text-primary text-small letter-spacing text-uppercase"><b><i class="fa fa-calendar-o"></i> {{Carbon\Carbon::parse($event->date)->format('jS M Y')}} <i class="fa fa-map-marker ml-4"></i> {{$event->location}}</b></p>
                                <p>{{substr($event->excerpt,0,200)}}@if(strlen($event->excerpt) > 200)...@endif</p>
                                <a href="/events/{{Carbon\Carbon::parse($event->date)->format('Y-m-d')}}/{{$event->slug}}">
                                    <button type="button" class="btn btn-primary">Find out more</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<div class="container-fluid position-relative py-5 mob-pt-0">
    <div class="row position-relative z-2 pb-5 mob-pb-3"> 
        <div class="container text-center">
            <p class="mimic-h2 mb-2">Latest News</p>
        </div>
        <news-home></news-home>
        <div class="col-12 text-center">
            <p class="text-larger text-statement-mob mb-2"><b>Want to read more?</b></p>
            <a href="/news">
                <button type="button" class="btn btn-primary">More News</button>
            </a>
        </div>
    </div>
</div>
<div class="container mb-5 position-relative z-2">
    <div class="row justify-content-center">
        <div class="col-lg-11 text-center">
            <p class="mimic-h2">Organisations <br class="d-md-none" />we work with</p>
            <p>We work with a wide range of organisations and service providers throughout Northern Ireland and in Great Britain who are committed to delivering support to veterans. Our current Veterans’ Places, Pathways and People Programme partners are represented below. To learn more about the veterans’ groups making a difference in your area, and to connect with your Veterans’ Champion, please <a href="/support-organisations"><u>click here</u></a>.</p>
        </div>
        <div class="col-12 mt-4">
            <picture>
                <source data-src="/img/charities/charities-desktop.webp" type="image/webp"/>
                <source data-src="/img/charities/charities-desktop.png" type="image/png"/>
                <img data-src="/img/charities/charities-desktop.png" alt="Supported Charities - NIVSO" class="w-100 h-auto d-none d-md-block lazy" width="1170" height="110" />
            </picture>
            <picture>
                <source data-src="/img/charities/charities-mob.webp" type="image/webp"/>
                <source data-src="/img/charities/charities-mob.png" type="image/png"/>
                <img data-src="/img/charities/charities-mob.png" alt="Supported Charities - NIVSO" class="w-100 h-auto d-md-none lazy" width="500" height="907" />
            </picture>
        </div>
    </div>
</div>
@endsection
@section('modals')
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="card px-4 py-5">
            <p>Please note this website will not be updated from 14th June 2024. The contact page and NIVSO office phone will remain live only until 30th June 2024. The website will remain visible for information until October 2024, at which time the website will be removed.</p>
            <p>For further information on veteran support in Northern Ireland, please visit <a href="https://nivco.co.uk">www.nivco.co.uk</a>  Thank you to our online community for your support.</p>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
    </div>

  </div>
</div>
@endsection
@section('scripts')

<script>
    $(document).ready(function(){
        $('#exampleModalCenter').modal('show');
    });
</script>
@endsection