@php
$page = 'Armed Forces Covenant';
$pagetitle = "Armed Forces Covenant - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "The Armed Forces Covenant is a promise by the nation ensuring that those who serve or who have served in the armed forces, and their families, are treated fairly.";
$pagetype = 'dark';
$pagename = 'armed-forces-covenant';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding mt-5">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row mt-5 mob-mt-0">
		<div class="container text-center text-lg-left mob-pt-5">
			<div class="row justify-content-center">
				<div class="col-lg-2 col-6 text-center text-lg-left">
					<picture>
		        <source src="/img/logos/afc.webp" type="image/webp"/>
		        <source src="/img/logos/afc.png" type="image/png"/>
		        <img src="/img/logos/afc.png" type="image/png" alt="Armed Forces Covenant Logo" class="w-100 h-auto d-inline-block afc-logo" width="200" height="1113" />
		      </picture>
		    </div>
		    <div class="col-lg-9 pl-5 mob-px-3 mob-mt-3">
					<h1 class="mb-4 mt-2 mob-mt-0">Armed Forces Covenant</h1>
					<p class="text-large">The Armed Forces Covenant is a promise by the nation ensuring that those who serve or who have served in the armed forces, and their families, are treated fairly.</p>
					<a href="https://www.armedforcescovenant.gov.uk/" target="_blank">
						<button class="btn btn-primary">Learn More</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 text-center text-lg-left position-relative z-2">
	<div class="row">
		<div class="col-lg-10 py-5">
			<p class="mimic-h3 mt-5">What is the Armed Forces Covenant?</p>
			<p>To those who proudly protect our nation, who do so with honour, courage, and commitment, the Armed Forces Covenant is the nation’s commitment to you.</p>
			<p class="mb-5">It is a pledge that together we acknowledge and understand that those who serve or who have served in the armed forces, and their families, should be treated with fairness and respect in the communities, economy and society they serve with their lives.</p>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aUrE8t22ba4?rel=0" allowfullscreen></iframe>
			</div>
		</div>
    <div class="col-lg-10">
			<p class="mimic-h3">What is being done?</p>
			<p>The covenant focusses on helping members of the armed forces community have&nbsp;the same access to government and commercial services and products&nbsp;as any other citizen.</p>
			<p>This <b><a href="https://www.armedforcescovenant.gov.uk/support-and-advice/" target="_blank">support</a></b> is provided in a number of areas including:</p>
			<ul class="text-left mob-pl-5">
			<li>education and family well-being</li>
			<li>having a home</li>
			<li>starting a new career</li>
			<li>access to healthcare</li>
			<li>financial assistance</li>
			<li>discounted services</li>
			</ul>
			<p>Further information about support services available to both <b><a href="https://www.gov.uk/topic/defence-armed-forces/support-services-military-defence-personnel-families" target="_blank">serving personnel and their families </a></b>,&nbsp;and <b><a href="https://www.gov.uk/topic/defence-armed-forces/support-services-veterans-families" target="_blank">veterans and their families </a></b>&nbsp;are provided on GOV.UK.</p>
			<p>Find information about&nbsp;<b><a href="https://www.gov.uk/browse/working/armed-forces" target="_blank">working, jobs and pensions for members of the armed forces and their families </a></b>.</p>
			<p>For more information about the background of of the Armed Forces Covenant go to the&nbsp;<b><a href="https://www.gov.uk/government/publications/armed-forces-covenant-2015-to-2020/armed-forces-covenant" target="_blank">covenant policy </a></b>.</p>
			<p>You can also read the<b><a href="https://www.gov.uk/government/publications/an-explanation-of-the-armed-forces-covenant" target="_blank">&nbsp;Armed Forces Covenant promise in full here </a></b>.</p>
		</div>
		<div class="col-lg-10 mt-5">
			<p class="mimic-h3">Who is involved?</p>
			<p>The Covenant supports serving personnel, service leavers, veterans, and&nbsp;their families, and is&nbsp;fulfilled by the different groups that have committed to making a difference.</p>
			<p>These include:</p>
			<ul class="text-left mob-pl-5">
			<li>Central government,&nbsp;overseen by the <b><a href="https://www.gov.uk/government/publications/ministerial-responsibility-for-covenant-and-veterans-issues" target="_blank">Ministerial Covenant and Veterans Board </a></b></li>
			<li>Single Services (Royal Navy, British Army, Royal Air Force)</li>
			<li>Businesses of all sizes</li>
			<li>Local government</li>
			<li>Charities</li>
			<li>Communities</li>
			<li>Cadet forces and their adult volunteers</li>
			</ul>
		</div>
		<div class="col-lg-10 mt-5">
			<p class="mimic-h3">Does it apply to you?</p>
			<p>If you are a member of the armed forces, a veteran, or a family member, the Armed Forces Covenant offers a wide variety of support to ensure you are being treated fairly.</p>
			<p>If you are a business, or a community organisation, you can find out how to show your support.</p>
			<p>This site will&nbsp;help direct you to the most relevant information regarding&nbsp;policies, services and projects that you can benefit from.</p>
			<p>Visit <b><a href="https://www.gov.uk/government/collections/armed-forces-covenant-supporting-information" target="_blank">Armed Forces Covenant guidance and support for information </a></b> on the breadth of the covenant and the support it provides.</p>
		</div>
		<div class="col-lg-10 mt-5">
			<p class="mimic-h3">Office for Veterans Affairs</p>
			<p>The UK Government has committed to making the UK the best country in the world to be a veteran by 2028. </p>
			<p>To do this it has created the Office for Veterans Affairs (OVA) which was launched by the Prime Minister in 2019, and a Minister for Veterans appointed in July 2022.  </p>
			<p>The work of the OVA can be found <b class="text-primary"><a href="https://www.gov.uk/government/organisations/office-for-veterans-affairs" target="_blank">here.</a></b></p>
		</div>
	</div>
</div>
@endsection