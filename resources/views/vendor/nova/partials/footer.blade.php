<p class="mt-8 text-center text-xs text-80">
    <a href="https://elementseven.co" class="text-primary dim no-underline" style="color: #000">Element Seven</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Element Seven Digital Limited
    <span class="px-1">&middot;</span>Nova 
    v{{ \Laravel\Nova\Nova::version() }}
</p>