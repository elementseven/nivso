@php
$page = 'Programmes';
$pagetitle = $programme->title . ' | NIVSO';
$metadescription = $programme->meta_description;
$keywords = $programme->keywords;
$pagetype = 'dark';
$pagename = 'programmes';
$ogimage = 'https://nivso.org.uk' . $programme->getFirstMediaUrl('normal');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('styles')
<style>
  p[data-f-id="pbf"]{
    display: none !important;
  }
</style>
@endsection
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding text-center text-lg-left">
  <div class="row mt-5 pt-5">
    <div class="container pt-5 mob-pt-0">
      <div class="row pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-10 pt-5 ipadp-pt-0 mob-pt-0 mob-px-4 mob-mb-3">
          <p class="text-title mb-2"><a href="{{route('programmes.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Back to browse programmes</b></a></p>
          <h1 class=" mb-4 blog-title">{{$programme->title}}</h1>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 text-center text-lg-left">
        <div class="col-lg-10 mob-mt-0 blog-body">
          {!!$programme->content!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1"><b>Share this programme:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($programme->title)}}&amp;summary={{urlencode($programme->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($programme->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($programme->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container-fluid bg-primary-lower-half mb-5 mb-5">
  <div class="row pb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p class="mimic-h2 mb-4">Related Programmes</p>
        </div>
        @foreach($others as $other)
        <div class="col-lg-4">
          <a href="/programmes/{{$other->id}}/{{$other->slug}}">
            <div class="card news-card transition post-card border-radius-0 text-dark px-0 py-0 overflow-hidden mb-4">
              <picture>
                <source srcset="{{$other->getFirstMediaUrl('programmes', 'featured-webp')}}" type="image/webp"/> 
                <source srcset="{{$other->getFirstMediaUrl('programmes', 'featured')}}" type="post.mimetype"/> 
                <img src="{{$other->getFirstMediaUrl('programmes', 'featured')}}" type="{{$other->getFirstMedia('programmes')->mime_type}}" alt="{{$other->title}}" class="lazy w-100 h-auto" width="460" height="322" />
              </picture>
              <div class="p-4 post-summary text-center">
                <p class="programme-title text-large text-title text-dark mb-2">{{substr($other->title,0,50)}}...</p>
                <p class="text-dark mb-3">{{substr($other->excerpt,0,120)}}...</p>
                <button class="btn btn-primary">View Programme</button>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
<charities></charities>
@endsection
@section('scripts')

<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection