@php
$page = 'Programmes & Committees ';
$pagetitle = "Programmes & Committees  - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Programmes & Committees from the Northern Ireland Veterans Support Office.";
$pagetype = 'dark';
$pagename = 'programmes';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5 mob-pb-4">
  <img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
  <div class="row pt-5">
    <div class="container text-center text-lg-left position-relative z-2">
      <h1 class="mb-4">Programmes & Committees </h1>
      <p>Many of our programmes are funded by the Armed Forces Covenant Fund Trust. You can read more about the AFCFT on their <a href="https://covenantfund.org.uk/" target="_blank"><b>website</b></a>, where you will see various programmes of support available and can learn about the impact that has been achieved.</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<programmes-index></programmes-index>
@endsection