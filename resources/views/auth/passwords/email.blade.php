@php
$page = 'Forgot your password';
$pagetitle = "Forgot your password - NIVSO";
$metadescription = "Reset your NIVSO password";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="container position-relative z-2">
            <div class="row justify-content-center">
                <div class="col-12 full-height">
                    <div class="d-table w-100 h-100">
                        <div class="d-table-cell w-100 h-100 align-middle">
                            <div class="">
                                <div class="card-body py-5 px-4">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <h2 class="text-center blog-title mb-4">Reset your password</h2>
                                    <form method="POST" action="{{ route('password.email') }}">
                                        @csrf

                                        <div class="form-group row">
                                            

                                            <div class="col-lg-6 offset-lg-3 mb-3">
                                                <label for="email" class="">{{ __('E-Mail Address') }}</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email address">

                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-12 text-center">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Email Me Reset Link') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
