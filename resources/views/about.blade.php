@php
$page = 'About';
$pagetitle = "About - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "The Northern Ireland Veterans’ Support Office was established to develop the capacity to deliver the Armed Forces Covenant in Northern Ireland.";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5 text-center text-lg-left">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row pt-5">
		<div class="container">
			<h1 class="">About NIVSO</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 text-center text-lg-left">
	<div class="row">
		<div class="col-12 mb-5">
			<p class="text-large"><b>The Northern Ireland Veterans’ Support Office was established to develop the capacity to deliver the Armed Forces Covenant in Northern Ireland.</b></p>
			<p class="text-large mb-5">We achieve this by developing strong working links between Devolved Government Departments, Local Government Veterans’ Champions and the voluntary and charitable sector. This ensures that veterans living in Northern Ireland do not experience disadvantage as a result of their Service in the Armed Forces and have the same access to services and support as any other citizen.</p>
			
		</div>
		<div class="col-12">
			<p class="mimic-h3">How do we do it?</p>
			<p>The NIVSO has 5 main tasks:</p>
			<ul class="text-left">
				<li><p>To be a trusted point of contact for veterans whose needs are not being met.</p></li>
				<li><p>To be a single point of contact for all organisations providing services for veterans.</p></li>
				<li><p>To develop a coherent and coordinated approach to support veterans.</p></li>
				<li><p>To increase the quantity and scope of services available by promoting and guiding access to Armed Forces Covenant Trust Funding.</p></li>
				<li><p>To achieve and monitor outcomes in all the above and communicate these outcomes appropriately.</p></li>
			</ul>
		</div>
	</div>
	<div class="row py-5 mt-5 mob-mt-0 ipadp-mt-0">
		<div class="col-lg-6 d-lg-none">
			<picture>
				<source src="/img/about/about-3-hex-left.png" type="image/png"/>
				<source src="/img/about/about-3-hex-left.webp" type="image/webp"/>
				<img src="/img/about/about-3-hex-left.png" width="533" height="570" class="w-100 h-auto mb-3" alt="About NIVSO"/>
			</picture>
		</div>
		<div class="col-lg-6">
			<p class="mimic-h3 mt-5">A trusted point of contact for veterans</p>
			<p>Veterans living in Northern Ireland may contact the NIVSO directly if they have a problem in accessing services or support.  The NIVSO is uniquely positioned to address these issues through links with Devolved Government Departments, Local Government Veterans’ Champions and the voluntary and charitable sector organisations who are responsible for delivering services to support veterans in Northern Ireland.  The NIVSO will contact the appropriate organisation or organisations to help, and will monitor the outcomes and follow up as needed.</p>     
		</div>
		<div class="col-lg-6 d-none d-lg-block px-5 mob-px-3">
			<picture>
				<source src="/img/about/about-3-hex-left.png" type="image/png"/>
				<source src="/img/about/about-3-hex-left.webp" type="image/webp"/>
				<img src="/img/about/about-3-hex-left.png" width="533" height="570" class="w-100 h-auto mb-3" alt="About NIVSO"/>
			</picture>
		</div>
	</div>
	<div class="row justify-content-center py-5 mob-pt-0">
		<div class="col-md-8 col-lg-6 px-5 mob-px-3 mob-mb-5 ipadp-mb-5">
			<picture>
				<source src="/img/about/about-3-hex-right.png" type="image/png"/>
				<source src="/img/about/about-3-hex-right.webp" type="image/webp"/>
				<img src="/img/about/about-3-hex-right.png" width="533" height="570" class="w-100 h-auto mb-3" alt="About NIVSO"/>
			</picture>
		</div>
		<div class="col-lg-6">
			<p class="mimic-h3 mt-5 pt-5 ipad-pt-0 ipad-mt-0 mob-pt-0 mob-mt-0">A single point of contact for all organisations</p>
			<p>The NIVSO has fostered strong relationships with a number of organisations and other key stakeholders in veteran services to ensure that they support the development and delivery of the Covenant.  The provision of advice, information, guidance, oversight and training to organisations will bring improvements to individuals’ abilities to access services and will broaden the scope of support in Northern Ireland. </p>
		</div>
		<div class="col-12 mt-5 ipad-pt-0">
			<p class="mimic-h3 mt-5">A coherent and coordinated approach</p>
			<p>The NIVSO plays a key role in building coherence and co-ordination among services in Northern Ireland through our facilitation of the Northern Ireland Veterans’ Support Committee (NIVSC). </p>
			<p>The NIVSC has been set up in Northern Ireland, under Ministerial guidance, to improve cooperation between those organisations who are permanently based in Northern Ireland and are committed to delivering practical support to veterans. The committee is also designed to act as a pool of expertise which can be consulted with a view to providing a consensus on veterans’ issues in Northern Ireland for policy makers and interested parties. We provide the coordination and national ‘voice’ for the NIVSC.</p>	
			<div class="table-responsive table-striped my-5">
			  <table class="table text-left">
			  	<thead>
				    <tr>
				      <th scope="col">Core Members</th>
				      <th scope="col">Associate Members currently include:</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <td><a href="https://blesma.org/" target="_blank"><u>BLESMA</u></a></td>
				      <td>38 (Irish) Brigade</td>
				    </tr>
				    <tr>
				      <td><a href="https://www.blindveterans.org.uk/" target="_blank"><u>Blind Veterans UK</u></a></td>
				      <td>ABF The Soldiers’ Charity</td>
				    </tr>
				    <tr>
				      <td><a href="https://combatstress.org.uk/" target="_blank"><u>Combat Stress</u></a></td>
				      <td>Andy Allen Veterans Support (AAVS)</td>
				    </tr>
				    <tr>
				      <td><a href="https://www.rfea.org.uk/" target="_blank"><u>Regular Forces Employment Association (RFEA)</u></a></td>
				      <td>Ashes To Gold</td>
				    </tr>
				    <tr>
				      <td><a href="http://www.reservesandcadetsni.org.uk/" target="_blank"><u>Reserve Forces and Cadets Association (NI) (RFCA (NI))</u></a></td>
				      <td>Brooke House</td>
				    </tr>
				    <tr>
				      <td><a href="https://rafa.org.uk/" target="_blank"><u>Royal Air Forces Association (RAFA)</u></a></td>
				      <td>Decorum NI</td>
				    </tr>
				    <tr>
				      <td><a href="https://royal-naval-association.co.uk/" target="_blank"><u>Royal Naval Association (RNA)</u></a></td>
				      <td>Help for Heroes (H4H)</td>
				    </tr>
				    <tr>
				      <td><a href="https://www.ssafa.org.uk/" target="_blank"><u>SSAFA</u></a></td>
				      <td>MAPS of West Tyrone</td>
				    </tr>
				    <tr>
				      <td><a href="https://thenotforgotten.org/" target="_blank"><u>The Not Forgotten Association (NFA)</u></a></td>
				      <td>MAST</td>
				    </tr>
				    <tr>
				      <td><a href="http://branches.britishlegion.org.uk/branches/brittany/about-trbl" target="_blank"><u>The Royal British Legion (TRBL)</u></a></td>
				      <td>MUVE</td>
				    </tr>
				    <tr>
				      <td><a href="https://aftercareservice.org/" target="_blank"><u>UDR & R IRISH (Home Service) Aftercare Service</u></a></td>
				      <td>Out of The Shadows</td>
				    </tr>
				    <tr>
				      <td><a href="https://www.gov.uk/government/organisations/veterans-advisory-and-pensions-committees-x13/about" target="_blank"><u>Veterans Advisory and Pensions Committee (VA&PC)</u></a></td>
				      <td>RAF Benevolent Fund</td>
				    </tr>
				    <tr>
				      <td><a href="https://www.gov.uk/government/organisations/veterans-uk" target="_blank"><u>Veterans UK</u></a></td>
				      <td>Regimental Associations</td>
				    </tr>
				    <tr>
				      <td></td>
				      <td>South East Fermanagh Foundation (SEFF)</td>
				    </tr>
				    <tr>
				      <td></td>
				      <td>The Beekeepers Lodge</td>
				    </tr>
				    <tr>
				      <td></td>
				      <td>The Castlehill Foundation</td>
				    </tr>
				    <tr>
				      <td></td>
				      <td>The Ely Centre</td>
				    </tr>
				    <tr>
				      <td></td>
				      <td>The War Widows’ Association</td>
				    </tr>
				  </tbody>
			  </table>
			</div>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-6">
			<p class="mimic-h3">Promoting and guiding access to Armed Forces Covenant Trust Funding</p>
			<p>In addition to working closely with a range of existing veteran support services in Northern Ireland, we also play a role in the development of new local services by encouraging and facilitating funding applications to the Armed Forces Covenant Trust Fund.</p>
			<p>There are a number of large health and wellbeing programmes within the fund, which we promote and work to facilitate collaborative bids from Northern Ireland-based organisations.  However, the fund also offers a local grants programme which awards payments for smaller projects that support community integration or local delivery of services to veterans. We can offer advice and guidance on applications to the fund. </p> 
		</div>
		<div class="col-md-8 col-lg-6 px-5 mob-px-3 mob-mt-5 ipad-pt-5">
			<picture>
				<source src="/img/home/brit-vet.png" type="image/png"/>
				<source src="/img/home/brit-vet.webp" type="image/webp"/>
				<img src="/img/home/brit-vet.png" width="533" height="570" class="w-100 h-auto mb-3" alt="About NIVSO 3"/>
			</picture>
		</div>
		<div class="col-12 mt-5 mob-mt-0">
			<p class="mimic-h3 mt-5">Achieve, monitor and communicate outcomes</p>
			<p>Outcomes will be achieved through liaison with the appropriate charity or service provider or by making bespoke arrangements for individual veterans.  Outcomes will be recorded and this in turn will inform best practice and the development of protocols, which will be shared widely among members of the NIVSC and other service providers. Noteworthy outcomes and events will be shared on our website.  Ultimately all outcomes will, in some way, inform future policy and strategy.</p>
			<picture>
				<source src="/img/about/about-5-hex.png" type="image/png"/>
				<source src="/img/about/about-5-hex.webp" type="image/webp"/>
				<img src="/img/about/about-5-hex.png" width="533" height="570" class="w-100 h-auto my-5" alt="About NIVSO 2"/>
			</picture>
		</div>
	</div>
</div>
@endsection