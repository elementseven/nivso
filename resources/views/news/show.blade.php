@php
$page = 'News';
$pagetitle = $post->title . ' | Lorem Ipsum';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'dark';
$pagename = 'news';
$ogimage = 'https://nivso.org.uk' . $post->getFirstMediaUrl('normal');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('styles')
<style>
  p[data-f-id="pbf"]{
    display: none !important;
  }
</style>
@endsection
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0 text-center text-lg-left">
  <div class="row mt-5 pt-5">
    <div class="container pt-5 mob-pt-0">
      <div class="row justify-content-center pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-12 pt-5 ipadp-pt-0 mob-pt-0 mob-px-4 mob-mb-3">
          <p class="text-title mb-0"><a href="{{route('news.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Back to browse news</b></a></p>
          <h1 class="mb-3 blog-title">{{$post->title}}</h1>
          <p class="text-light-grey text-title mb-0">{{\Carbon\Carbon::parse($post->created_at)->format('jS M Y')}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4 text-center text-lg-left">
    <div class="row pb-5">
        <div class="col-12 mob-mt-0 blog-body">
          {!!$post->content!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1"><b>Share this article:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container-fluid bg-primary-lower-half mb-5 mb-5">
  <div class="row pb-5">
    <div class="container text-center text-lg-left">
      <div class="row">
        <div class="col-12">
          <p class="mimic-h2 mb-4">Related Articles</p>
        </div>
        @foreach($others as $other)
        <div class="col-lg-4 mob-mb-4">
          <a href="/news/{{\Carbon\Carbon::parse($other->created_at)->format('Y-m-d')}}/{{$other->slug}}">
            <div class="card transition post-card border-radius-0 text-dark px-0 py-0 overflow-hidden">
              <picture>
                <source srcset="{{$other->getFirstMediaUrl('news', 'normal-webp')}}" type="image/webp"/> 
                <source srcset="{{$other->getFirstMediaUrl('news', 'normal')}}" type="post.mimetype"/> 
                <img src="{{$other->getFirstMediaUrl('news', 'normal')}}" type="{{$other->getFirstMedia('news')->mime_type}}" alt="{{$other->title}}" class="lazy w-100 h-auto" width="460" height="322" />
              </picture>
              <div class="p-4 post-summary text-center">
                <p class="text-uppercase letter-spacing text-primary text-small mb-0"><b>{{\Carbon\Carbon::parse($other->created_at)->format('jS M Y')}}</b></p>
                <p class="post-title text-large text-title text-dark mb-2">{{substr($other->title,0,50)}}...</p>
                <p class="text-dark mb-3">{{substr($other->excerpt,0,120)}}...</p>
                <button class="btn btn-primary">Read Article</button>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')

<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection