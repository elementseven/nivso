@php
$page = 'Homepage';
$pagetitle = "News - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua.";
$pagetype = 'dark';
$pagename = 'news';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5 pb-5 mob-pb-4 text-center text-lg-left">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row pt-5">
		<div class="container">
			<h1 class="mb-0">Latest News</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<news-index :categories="{{$categories}}"></news-index>
@endsection