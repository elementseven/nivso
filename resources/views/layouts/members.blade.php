<!DOCTYPE html>
<html id="wav-html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="canonical" href="https://tourguidesni.com/">
  <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/img/favicon/site.webmanifest">
	<link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>
  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="NIVSO">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:image:secure" content="{{$ogimage}}">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:image:width" content="1200" /> 
  <meta property="og:image:height" content="627" />
  <meta property="og:updated_time" content="2021-04-14T21:23:54+0000" />
  <meta property="og:description" content="{{$metadescription}}">
  <meta name="description" content="{{$metadescription}}">
  <link rel="preload" as="font" href="/fonts/fontawesome-webfont.woff2?af7ae505a9eed503f8b8e6982036873e" type="font/woff2" crossorigin="anonymous">
  <link href="{{ mix('css/app.min.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "00447704575932",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
@yield('head_section')
</head>  
<body id="nivso-body" class="front page-{{$pagename}}">

<div id="main-wrapper">
  <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
    <main-menu :pagetype="'{{$pagetype}}'" :loggedin="{{Auth::check()}}"></main-menu>
    <mobile-menu :pagetype="'{{$pagetype}}'" :loggedin="{{Auth::check()}}"></mobile-menu>
    @yield('header')
    <main id="content" style="z-index: 2;" class="">
      <div id="menu-trigger"></div>
      <div id="menu-trigger-2"></div>
      @yield('content')
    </main>
    <site-footer></site-footer>
    @yield('modals')
    <loader></loader>

  </div>
  <div id="menu_body_hide"></div>
</div>
@yield('prescripts')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@yield('scripts')
<script>
  window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#004dac",
         "text": "#ffffff"
       },
       "button": {
         "background": "#FBCF00",
         "text": "#030303",
         "border-radius": "30px"
       }
     }
   })});
 </script>
</body>
</html>