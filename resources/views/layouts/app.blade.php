<!DOCTYPE html>
<html id="wav-html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="canonical" href="https://nivso.org.uk/">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>
  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Northern Ireland Veterans Support Office">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:image:secure" content="{{$ogimage}}">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:image:width" content="1200" /> 
  <meta property="og:image:height" content="627" />
  <meta property="og:updated_time" content="2021-04-14T21:23:54+0000" />
  <meta property="og:description" content="{{$metadescription}}">
  <meta name="description" content="{{$metadescription}}">
  <link rel="preload" as="font" href="/fonts/fontawesome-webfont.woff2?af7ae505a9eed503f8b8e6982036873e" type="font/woff2" crossorigin="anonymous">
  <link href="{{ mix('css/app.min.css') }}" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "00447704575932",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
@yield('head_section')
</head>  
<body id="nffc-body" class="front page-{{$pagename}}">
<div id="main-wrapper">
  <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
    <main-menu :pagetype="'{{$pagetype}}'"></main-menu>
    <mobile-menu :pagetype="'{{$pagetype}}'"></mobile-menu>
    @yield('header')
    <main id="content" style="z-index: 2;" class="">
      <div id="menu-trigger"></div>
      <div id="menu-trigger-2"></div>
      @yield('content')
    </main>
    <site-footer></site-footer>
    @yield('modals')
    <loader></loader>
  </div>
  <div id="menu_body_hide"></div>
</div>
@yield('prescripts')
<script src="{{ mix('/js/manifest.js') }}"></script>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@yield('scripts')
<script>
  window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#1C2D4B",
         "text": "#ffffff"
       },
       "button": {
         "background": "#FBDC64",
         "text": "#1C2D4B",
         "border-radius": "0"
       }
     }
   })});
 </script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5M5RLVT');</script>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5M5RLVT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
</body>
</html>