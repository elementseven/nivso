@php
$page = 'Contact';
$pagetitle = "Contact - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Contact NIVSO.";
$pagetype = 'dark';
$pagename = 'contact';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5 mob-mb-0">
	<div class="row">
		<div class="container text-center text-lg-left">
			<h1 class="mb-3 pt-5">Contact NIVSO</h1>
			<p>Use the contact form below to send us a message!</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-4 pb-5 mb-5 position-relative z-2">
	<div class="row">
		<div class="col-lg-12">
      <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'" style="min-height: 450px;"></contact-page-form>
		</div>
	</div>
</div>
@endsection
@section('modals')
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="card px-4 py-5">
            <p>Please note this website will not be updated from 14th June 2024. The contact page and NIVSO office phone will remain live only until 30th June 2024. The website will remain visible for information until October 2024, at which time the website will be removed.</p>
            <p>For further information on veteran support in Northern Ireland, please visit <a href="https://nivco.co.uk">www.nivco.co.uk</a>  Thank you to our online community for your support.</p>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
    </div>

  </div>
</div>
@endsection
@section('scripts')

<script>
    $(document).ready(function(){
        $('#exampleModalCenter').modal('show');
    });
</script>
@endsection