@php
$page = 'Mental Health';
$pagetitle = "Mental Health & Peer Support - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Veterans and their families often experience life events which impact on their mental health. Asking for help can seem daunting, but there are so many people who will support you if you choose to take that first step.";
$pagetype = 'dark';
$pagename = 'mental-health';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative top-padding text-center text-lg-left">
	<div class="row pt-5 ">
		<div class="col-lg-10">
			<h1 class="pt-5 mob-pt-0 mb-4">Mental Health & Peer Support <br/>Across Northern Ireland</h1>
			<p>Veterans and their families often experience life events which impact on their mental health. Asking for help can seem daunting, but there are so many people who will support you if you choose to take that first step.</p>
			<p>Through our links in the community, voluntary, private and statutory sectors we have also identified veteran-friendly organisations in Northern Ireland that welcome veterans to join in and take part in their activities. From archery to golf, embroidery to filmmaking, there are activities on offer to suit everyone's taste.</p>
			<p>Some of these organisations have received direct funding to offer veteran-specific programmes, others run year-round activities that are also open to the public.</p>
			<p><b>Click on a document related to your council area below to download our helpful guide to services in your area.</b></p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 my-5">
	<div class="row half_row mb-5 mob-mb-0">	
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/antrim-and-newtownabbey-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/antrim-and-newtownabbey-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/antrim-and-newtownabbey-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/antrim-and-newtownabbey-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="antrim-and-newtownabbey-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/ards-and-north-down-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/ards-and-north-down-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/ards-and-north-down-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/ards-and-north-down-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="ards-and-north-down-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/armagh-banbridge-and-craigavon-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/armagh-banbridge-and-craigavon-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/armagh-banbridge-and-craigavon-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/armagh-banbridge-and-craigavon-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="armagh-banbridge-and-craigavon-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/belfast-city-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/belfast-city-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/belfast-city-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/belfast-city-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="belfast-city-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/causeway-coast-and-glens-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/causeway-coast-and-glens-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/causeway-coast-and-glens-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/causeway-coast-and-glens-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="causeway-coast-and-glens-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/derry-city-and-strabane-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/derry-city-and-strabane-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/derry-city-and-strabane-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/derry-city-and-strabane-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="derry-city-and-strabane-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/fermanagh-and-omagh-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/fermanagh-and-omagh-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/fermanagh-and-omagh-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/fermanagh-and-omagh-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="fermanagh-and-omagh-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/lisburn-and-castlereagh-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/lisburn-and-castlereagh-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/lisburn-and-castlereagh-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/lisburn-and-castlereagh-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="lisburn-and-castlereagh-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/mid-and-east-antrim-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/mid-and-east-antrim-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/mid-and-east-antrim-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/mid-and-east-antrim-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="mid-and-east-antrim-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/mid-ulster-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/mid-ulster-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/mid-ulster-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/mid-ulster-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="mid-ulster-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/newry-mourne-and-down-booklet.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/newry-mourne-and-down-booklet.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/newry-mourne-and-down-booklet.webp" type="image/webp"/>
					<img src="/img/mental-health/newry-mourne-and-down-booklet.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="newry-mourne-and-down-booklet"/>
				</picture>
			</a>
		</div>
		<div class="col-lg-3 col-6 half_col">
			<a href="/docs/mental-health-and-peer-support/ni-veterans-mental-health-guide-cover-page.pdf" target="_blank">
				<picture>
					<source src="/img/mental-health/ni-veterans-mental-health-guide-cover-page.jpg" type="image/jpeg"/>
					<source src="/img/mental-health/ni-veterans-mental-health-guide-cover-page.webp" type="image/webp"/>
					<img src="/img/mental-health/ni-veterans-mental-health-guide-cover-page.jpg" width="533" height="570" class="w-100 h-auto mb-4 shadow" alt="ni-veterans-mental-health-guide-cover-page"/>
				</picture>
			</a>
		</div>
	</div>
</div>
@endsection