@php
$page = 'Events';
$pagetitle = "Events - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Upcoming Events from the Northern Ireland Veterans Support Office";
$pagetype = 'dark';
$pagename = 'events';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5 pb-5 mob-pb-4">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex d-lg-none" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row pt-5">
		<div class="container text-center text-lg-left">
			<h1 class="mb-0">Events</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<events-index></events-index>
@endsection