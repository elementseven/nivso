@php
$page = 'events';
$pagetitle = $event->name . ' | NIVSO';
$metadescription = $event->meta_description;
$keywords = $event->keywords;
$pagetype = 'dark';
$pagename = 'events';
$ogimage = 'https://nivso.org.uk/' . $event->getFirstMediaUrl('normal');
@endphp
@extends('layouts.app', ['pagename' => $pagename, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('styles')
<style>
  p[data-f-id="pbf"]{
    display: none !important;
  }
</style>
@endsection
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0">
  <div class="row mt-5 pt-5">
    <div class="container pt-5 mob-pt-0">
      <div class="row justify-content-center pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-6 text-center">
          <p class="text-title mb-0 z-2 position-relative d-lg-none"><a href="{{route('events.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Back to browse events</b></a></p>
          <h1 class="mb-4 event-nam d-lg-none">{{$event->name}}</h1>
          <picture>
                <source src="{{$event->getFirstMediaUrl('events','normal-webp')}}" type="image/webp"/>
                <source src="{{$event->getFirstMediaUrl('events','normal')}}" type="{{$event->getFirstMedia('events')->mime_type}}"/>
                <img src="{{$event->getFirstMediaUrl('events','normal')}}" alt="{{$event->name}} - Events NIVSO" class="w-100 h-auto" width="536" height="403" />
            </picture>
        </div>
        <div class="col-lg-6 pt-5 ipadp-pt-0 mob-pt-0 mob-px-4 mob-mb-3 text-center text-lg-left">
          <p class="text-title mb-0 z-2 position-relative d-none d-lg-block"><a href="{{route('events.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Back to browse events</b></a></p>
          <h1 class="mb-3 event-name d-none d-lg-block">{{$event->name}}</h1>
          <p class="text-primary text-title mb-0 mob-mt-3"><i class="fa fa-calendar-o mr-2"></i>{{\Carbon\Carbon::parse($event->date)->format('jS M Y H:i')}} <i class="fa fa-map-marker ml-4"></i> {{$event->location}}</p>
          <p class="mt-3">{{$event->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4 text-center text-lg-left">
    <div class="row pb-5">
        <div class="col-12 mob-mt-0 blog-body">
          {!!$event->description!!}
        </div>
        <div class="col-12 mt-5 text-center text-lg-left">
          <p class="mb-1"><b>Share this event:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;name={{urlencode($event->name)}}&amp;summary={{urlencode($event->name)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($event->name)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($event->name)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
@if(count($others))
<div class="container-fluid bg-primary-lower-half mb-5 mb-5 text-center text-lg-left position-relative z-2">
  <div class="row pb-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12">
          <p class="mimic-h2 mb-4">More Events</p>
        </div>
        @foreach($others as $other)
        <div class="col-lg-4 col-md-6 mb-4">
          <a href="/events/{{\Carbon\Carbon::parse($other->date)->format('Y-m-d')}}/{{$other->slug}}">
            <div class="card transition post-card border-radius-0 text-dark px-0 py-0 overflow-hidden">
              <picture>
                <source srcset="{{$other->getFirstMediaUrl('events', 'normal-webp')}}" type="image/webp"/> 
                <source srcset="{{$other->getFirstMediaUrl('events', 'normal')}}" type="post.mimetype"/> 
                <img src="{{$other->getFirstMediaUrl('events', 'normal')}}" type="{{$other->getFirstMedia('events')->mime_type}}" alt="{{$other->name}}" class="lazy w-100 h-auto" width="460" height="322" />
              </picture>
              <div class="py-4 px-3 post-summary text-center">
                
                <p class="post-name text-large text-title text-dark mb-2">{{$other->name}}</p>
                <p class="text-uppercase letter-spacing text-primary text-small mb-2"><b><i class="fa fa-calendar-o mr-2"></i>{{\Carbon\Carbon::parse($other->date)->format('jS M Y H:i')}}<br/><i class="fa fa-map-marker"></i> {{$other->location}}</b></p>
                <p class="text-dark mb-4">{{substr($other->excerpt,0,120)}}...</p>
                <button class="btn btn-primary">View Event</button>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('scripts')

<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection