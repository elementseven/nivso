@php
$page = 'Members Area';
$pagetitle = "Members Area | NIVSO";
$metadescription = "lorem";
$pagetype = 'light';
$pagename = 'dashboard';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.members', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 mob-pb-0">
    <div class="row pt-5 mt-5 mob-mt-0">
        <div class="col-lg-8 position-relative z-2 pt-5 mt-5 mob-mt-0">
            <p class="mb-0 text-uppercase text-primary letter-spacing">Welcome to your</p>
            <h1 class="mb-3">Members Area</h1>
            <p>Please browse the information available and let us know if we are missing anything you need.</p>
        </div>
    </div>
</header>
@endsection
@section('content')
<members-updates-index></members-updates-index>
@endsection
@section('scripts')

@endsection