@php
$page = 'Support Organisations';
$pagetitle = "Support Organisations - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "We work with a wide range of organisations and service providers throughout Northern Ireland and in Great Britain who are committed to delivering support to veterans.";
$pagetype = 'dark';
$pagename = 'charities';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5">
	<div class="row">
		<div class="container mb-4 mob-px-4 text-center text-lg-left">
			<h1 class="mb-4 pt-5">Support Organisations</h1>
			<p>From mental health through to peer support services, these trusted organisations work across Northern Ireland and can provide support for veterans and their families in times of need.</p>
			<p>Select your local council area to see what other organisations exist in your area, and get connected.</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<charities-index :councils="{{$councils}}"></charities-index>
@endsection