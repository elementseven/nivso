@php
$page = 'Local Heroes';
$pagetitle = "Local Heroes - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Local Heroes from the Northern Ireland Veterans Support Office";
$pagetype = 'dark';
$pagename = 'local-heroes';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5 pb-5 mob-pt-5">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row pt-5 mob-pt-0">
		<div class="container text-center text-lg-left">
			<h1 class="">Local Heroes</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<heroes-index></heroes-index>
@endsection