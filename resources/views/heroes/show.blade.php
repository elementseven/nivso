@php
$page = 'Local Hero';
$pagetitle = $hero->name . ' | Northern Ireland Veterans Support Office | NIVSO';
$metadescription = $hero->excerpt;
$pagetype = 'dark';
$pagename = 'local-hero';
$ogimage = 'https://nivso.org.uk' . $hero->getFirstMediaUrl('normal');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('styles')
<style>
  p[data-f-id="pbf"]{
    display: none !important;
  }
  
  .fr-video{
    display: block;
    overflow: hidden;
    padding: 0;
    position: relative;
    width: 100%;
  }
  .fr-video:before {
    padding-top: 56.25%;
    content: "";
    display: block;
  }
  .fr-video iframe{
    border: 0;
    bottom: 0;
    height: 100% !important;
    left: 0;
    position: absolute;
    top: 0;
    width: 100% !important;
  }
  .fr-img-caption .fr-inner{
    font-size: 16px;
    margin-top: 6px;
    margin-bottom: 1rem !important;
    display: block;
    color: #666;
  }
</style>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0">
  <img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern">
  <div class="row mt-5 pt-5 ipadp-pt-0">
    <div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 text-center text-lg-left">
      <div class="row justify-content-center pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-6">
          <p class="text-title mb-2 mob-mb-0 d-lg-none"><a href="{{route('heroes.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Back to all heroes</b></a></p>
          <h1 class="mb-3 blog-title d-lg-none">{{$hero->name}}</h1>
          <picture>
            <source srcset="{{$hero->getFirstMediaUrl('heroes','normal-webp')}}" type="image/webp"/> 
            <source srcset="{{$hero->getFirstMediaUrl('heroes','normal')}}" type="{{$hero->getFirstMedia('heroes')->mime_type}}"/> 
            <img src="{{$hero->getFirstMediaUrl('heroes','normal')}}" type="{{$hero->getFirstMedia('heroes')->mime_type}}" alt="{{$hero->name}} | NIVSO" class="lazy w-100 shadow"/>
          </picture>
        </div>
        <div class="col-lg-6 pt-5 pl-5 mob-pt-4 mob-px-3">
          <p class="text-title mb-2 d-none d-lg-block"><a href="{{route('heroes.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Back to all heroes</b></a></p>
          <h1 class="mb-3 blog-title d-none d-lg-block">{{$hero->name}}</h1>
          <p>{{$hero->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 text-center text-lg-left">
        <div class="col-12 mob-mt-0 blog-body">
          {!!$hero->description!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1"><b>Share this hero:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($hero->name)}}&amp;summary={{urlencode($hero->name)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($hero->name)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($hero->name)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container-fluid bg-primary-lower-half mb-5 mb-5 position-relative z-2">
  <div class="row pb-5">
    <div class="container text-center text-lg-left">
      <div class="row justify-content-center">
        <div class="col-12">
          <p class="mimic-h2 mb-4">More Local Heroes</p>
        </div>
        @foreach($others as $other)
        <div class="col-md-6 col-lg-4">
          
            <div class="card transition post-card border-radius-0 text-dark px-0 py-0 overflow-hidden">
              <picture>
                <source srcset="{{$other->getFirstMediaUrl('heroes', 'normal-webp')}}" type="image/webp"/> 
                <source srcset="{{$other->getFirstMediaUrl('heroes', 'normal')}}" type="post.mimetype"/> 
                <img src="{{$other->getFirstMediaUrl('heroes', 'normal')}}" type="{{$other->getFirstMedia('heroes')->mime_type}}" alt="{{$other->title}}" class="lazy w-100 h-auto" width="460" height="322" />
              </picture>
              <div class="p-4 post-summary text-center">
                <p class="post-title text-title text-dark mb-2">{{substr($other->name,0,36)}}</p>
                <p class="text-dark mb-3">{{substr($other->excerpt,0,80)}}...</p>
                <a href="/local-heroes/{{$other->id}}/{{$other->slug}}">
                  <button class="btn btn-primary">Read More</button>
                </a>
              </div>
            </div>
          
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection