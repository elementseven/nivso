@php
$page = 'Privacy Policy';
$pagetitle = "Privacy Policy - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "NIVSO Privacy Policy";
$pagetype = 'dark';
$pagename = 'tandcs';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding mt-5">
	<div class="row">
		<div class="container  mob-px-4 text-center text-lg-left">
			<h1 class="mb-4 pt-5">Privacy Policy</h1>
			<div>Last updated 9 May 2019</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row">
		<div class="col-12">
			<div>
				<p class="mt-5 mimic-h3">Owner and Data Controller</p>NIVSO 25 Windsor Park Belfast BT9 6FR&nbsp;<strong>Owner contact email:</strong> info@nivso.org.uk
			</div>
			<div>
				<p class="mt-5 mimic-h3">Types of Data collected</p>
				<p>Among the types of Personal Data that this Application collects, by itself or through third parties, there are: first name; last name; phone number; email address. Complete details on each type of Personal Data collected are provided in the dedicated sections of this privacy policy or by specific explanation texts displayed prior to the Data collection. Personal Data may be freely provided by the User, or, in case of Usage Data, collected automatically when using this Application. Unless specified otherwise, all Data requested by this Application is mandatory and failure to provide this Data may make it impossible for this Application to provide its services. In cases where this Application specifically states that some Data is not mandatory, Users are free not to communicate this Data without consequences to the availability or the functioning of the Service. Users who are uncertain about which Personal Data is mandatory are welcome to contact the Owner. Any use of Cookies &ndash; or of other tracking tools &ndash; by this Application or by the owners of third-party services used by this Application serves the purpose of providing the Service required by the User, in addition to any other purposes described in the present document and in the Cookie Policy, if available. Users are responsible for any third-party Personal Data obtained, published or shared through this Application and confirm that they have the third party&apos;s consent to provide the Data to the Owner.</p>
			</div>
			<div>
				<p class="mt-5 mimic-h3">Mode and place of processing the Data</p>
				<p class="mt-4 mimic-h4">Methods of processing</p>
				<p>The Owner takes appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data. The Data processing is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Owner, in some cases, the Data may be accessible to certain types of persons in charge, involved with the operation of this Application (administration, sales, marketing, legal, system administration) or external parties (such as third-party technical service providers, mail carriers, hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner. The updated list of these parties may be requested from the Owner at any time.</p>
				<p class="mt-4 mimic-h4">Legal basis of processing</p>
				<p>The Owner may process Personal Data relating to Users if one of the following applies:</p>
				<ul>
					<li><p>Users have given their consent for one or more specific purposes. Note: Under some legislations the Owner may be allowed to process Personal Data until the User objects to such processing (&ldquo;opt-out&rdquo;), without having to rely on consent or any other of the following legal bases. This, however, does not apply, whenever the processing of Personal Data is subject to European data protection law;</p></li>
					<li><p>provision of Data is necessary for the performance of an agreement with the User and/or for any pre-contractual obligations thereof;</p></li>
					<li><p>processing is necessary for compliance with a legal obligation to which the Owner is subject;</p></li>
					<li><p>processing is related to a task that is carried out in the public interest or in the exercise of official authority vested in the Owner;</p></li>
					<li><p>processing is necessary for the purposes of the legitimate interests pursued by the Owner or by a third party.</p></li>
				</ul>
				<p>In any case, the Owner will gladly help to clarify the specific legal basis that applies to the processing, and in particular whether the provision of Personal Data is a statutory or contractual requirement, or a requirement necessary to enter into a contract.</p>
				<p class="mt-4 mimic-h4">Place</p>
				<p>The Data is processed at the Owner&apos;s operating offices and in any other places where the parties involved in the processing are located. Depending on the User&apos;s location, data transfers may involve transferring the User&apos;s Data to a country other than their own. To find out more about the place of processing of such transferred Data, Users can check the section containing details about the processing of Personal Data. Users are also entitled to learn about the legal basis of Data transfers to a country outside the European Union or to any international organization governed by public international law or set up by two or more countries, such as the UN, and about the security measures taken by the Owner to safeguard their Data. If any such transfer takes place, Users can find out more by checking the relevant sections of this document or inquire with the Owner using the information provided in the contact section.</p>

				<p class="mt-4 mimic-h4">Retention time</p>
				<p>Personal Data shall be processed and stored for as long as required by the purpose they have been collected for. Therefore:</p>
				<ul>
					<li><p>Personal Data collected for purposes related to the performance of a contract between the Owner and the User shall be retained until such contract has been fully performed.</p></li>
					<li><p>Personal Data collected for the purposes of the Owner&rsquo;s legitimate interests shall be retained as long as needed to fulfill such purposes. Users may find specific information regarding the legitimate interests pursued by the Owner within the relevant sections of this document or by contacting the Owner.</p></li>
				</ul>
				<p>The Owner may be allowed to retain Personal Data for a longer period whenever the User has given consent to such processing, as long as such consent is not withdrawn. Furthermore, the Owner may be obliged to retain Personal Data for a longer period whenever required to do so for the performance of a legal obligation or upon order of an authority. Once the retention period expires, Personal Data shall be deleted. Therefore, the right to access, the right to erasure, the right to rectification and the right to data portability cannot be enforced after expiration of the retention period.</p>
			</div>
			<div>
				<p class="mt-5 mimic-h3">The purposes of processing</p>
				<p>The Data concerning the User is collected to allow the Owner to provide its Services, as well as for the following purposes: Contacting the User. Users can find further detailed information about such purposes of processing and about the specific Personal Data used for each purpose in the respective sections of this document.</p>
			</div>
			<div>
				<p class="mt-5 mimic-h3">Detailed information on the processing of Personal Data</p>
				<p>Personal Data is collected for the following purposes and using the following services:</p>
				<ul>
					<li><p>Contacting the User</p></li>
				</ul>
			</div>
			<div>
				<p class="mt-5 mimic-h3">The rights of Users</p>
				<p>Users may exercise certain rights regarding their Data processed by the Owner. In particular, Users have the right to do the following:</p>
				<ul>
					<li><p><strong>Withdraw their consent at any time.</strong> Users have the right to withdraw consent where they have previously given their consent to the processing of their Personal Data.</p></li>
					<li><p><strong>Object to processing of their Data.</strong> Users have the right to object to the processing of their Data if the processing is carried out on a legal basis other than consent. Further details are provided in the dedicated section below.</p></li>
					<li><p><strong>Access their Data.</strong> Users have the right to learn if Data is being processed by the Owner, obtain disclosure regarding certain aspects of the processing and obtain a copy of the Data undergoing processing.</p></li>
					<li><p><strong>Verify and seek rectification.</strong> Users have the right to verify the accuracy of their Data and ask for it to be updated or corrected.</p></li>
					<li><p><strong>Restrict the processing of their Data.</strong> Users have the right, under certain circumstances, to restrict the processing of their Data. In this case, the Owner will not process their Data for any purpose other than storing it.</p></li>
					<li><p><strong>Have their Personal Data deleted or otherwise removed.</strong> Users have the right, under certain circumstances, to obtain the erasure of their Data from the Owner.</p></li>
					<li><p><strong>Receive their Data and have it transferred to another controller.</strong> Users have the right to receive their Data in a structured, commonly used and machine readable format and, if technically feasible, to have it transmitted to another controller without any hindrance. This provision is applicable provided that the Data is processed by automated means and that the processing is based on the User&apos;s consent, on a contract which the User is part of or on pre-contractual obligations thereof.</p></li>
					<li><p><strong>Lodge a complaint.</strong> Users have the right to bring a claim before their competent data protection authority.</p></li>
				</ul>
				<p class="mt-4 mimic-h4">Details about the right to object to processing</p>
				<p>Where Personal Data is processed for a public interest, in the exercise of an official authority vested in the Owner or for the purposes of the legitimate interests pursued by the Owner, Users may object to such processing by providing a ground related to their particular situation to justify the objection. Users must know that, however, should their Personal Data be processed for direct marketing purposes, they can object to that processing at any time without providing any justification. To learn, whether the Owner is processing Personal Data for direct marketing purposes, Users may refer to the relevant sections of this document.</p>
				<p class="mt-4 mimic-h4">How to exercise these rights</p>
				<p>Any requests to exercise User rights can be directed to the Owner through the contact details provided in this document. These requests can be exercised free of charge and will be addressed by the Owner as early as possible and always within one month.</p>
			</div>
			<div>
				<p class="mt-5 mimic-h3">Additional information about Data collection and processing</p>
				<p class="mt-4 mimic-h4">Legal action</p>
				<p>The User&apos;s Personal Data may be used for legal purposes by the Owner in Court or in the stages leading to possible legal action arising from improper use of this Application or the related Services. The User declares to be aware that the Owner may be required to reveal personal data upon request of public authorities.</p>
				<p class="mt-4 mimic-h4">Additional information about User&apos;s Personal Data</p>
				<p>In addition to the information contained in this privacy policy, this Application may provide the User with additional and contextual information concerning particular Services or the collection and processing of Personal Data upon request.</p>
				<p class="mt-4 mimic-h4">System logs and maintenance</p>
				<p>For operation and maintenance purposes, this Application and any third-party services may collect files that record interaction with this Application (System logs) use other Personal Data (such as the IP Address) for this purpose.</p>
				<p class="mt-4 mimic-h4">Information not contained in this policy</p>
				<p>More details concerning the collection or processing of Personal Data may be requested from the Owner at any time. Please see the contact information at the beginning of this document.</p>
				<p class="mt-4 mimic-h4">How &ldquo;Do Not Track&rdquo; requests are handled</p>
				<p>This Application does not support &ldquo;Do Not Track&rdquo; requests. To determine whether any of the third-party services it uses honor the &ldquo;Do Not Track&rdquo; requests, please read their privacy policies.</p>
				<p class="mt-4 mimic-h4">Changes to this privacy policy</p>
				<p>The Owner reserves the right to make changes to this privacy policy at any time by giving notice to its Users on this page and possibly within this Application and/or - as far as technically and legally feasible - sending a notice to Users via any contact information available to the Owner. It is strongly recommended to check this page often, referring to the date of the last modification listed at the bottom. Should the changes affect processing activities performed on the basis of the User&rsquo;s consent, the Owner shall collect new consent from the User, where required.</p>
			</div>
			
		</div>
	</div>
</div>
@endsection