@php
$page = 'Research';
$pagetitle = "Research - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "We are working to learn more about the support needs of Veterans in Northern Ireland. To achieve this, in 2015, Forces in Mind Trust (FiMT) commissioned a team of researchers based in Northern Ireland to conduct a large-scale study into the Health and Wellbeing of Veterans living in Northern Ireland.";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative top-padding my-5">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern"/>
	<div class="row pt-5">
		<div class="container text-center text-lg-left">
			<h1 class="">Research in Northern Ireland</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 text-center text-lg-left">
	<div class="row">
		<div class="col-12">
			<p class="text-large"><b>We are working to learn more about the support needs of Veterans in Northern Ireland. To achieve this, in 2015, Forces in Mind Trust (FiMT) commissioned a team of researchers based in Northern Ireland to conduct a large-scale study into the Health and Wellbeing of Veterans living in Northern Ireland.</b></p>
			<p class="text-large">The Northern Ireland Veterans’ Health and Wellbeing Study (NIVHWS) is a unique large-scale tri-service research project funded by the Forces in Mind Trust (FiMT) and the Big Lottery Fund.</p>
			<p class="text-large">The study began in 2015 and was completed in 2019. It was conducted by Prof Chérie Armour and a team of researchers from the Psychology Research Institute at Ulster University in partnership with the Northern Ireland Veterans Support Committee (NIVSC). You can click on the items below to read the results of this study.</p>
		</div>
	</div>
	<div class="row half_row">
		@foreach($northernireland as $research)
		<div class="col-lg-3 col-6 half_col">
			<a href="{{$research->getFirstMediaUrl('research-files')}}" target="_blank">
				<div class="card shadow p-0 mt-4">
					<picture>
		          <source src="{{$research->webo}}" type="image/webp"/>
		          <source src="{{$research->image}}" type="{{$research->mime}}"/>
		          <img src="{{$research->image}}" alt="NIVSO - {{$research->name}}" class="w-100" />
		      </picture>
					<p class="px-3 mt-3 mb-2 text-one text-dark"><b>{{$research->name}}</b></p>
					<p class="px-3 text-primary text-one"><u><b>Download</b></u> <i class="fa fa-cloud-download"></i></p>
				</div>
			</a>
		</div>
		@endforeach
	</div>
	<div class="row half_row mt-5 py-5">
		<div class="col-12 half_col">
			<p class="mimic-h2">Research from across the UK</p>
			<p class="text-large">We work closely with a number of UK-wide organisations to produce and promote the latest research in the Veterans' field. </p>
		</div>
		@foreach($unitedkingdom as $research)
		<div class="col-lg-3 col-6 half_col">
			<a href="{{$research->getFirstMediaUrl('research-files')}}" target="_blank">
				<div class="card shadow p-0 mt-4">
					<picture>
		          <source src="{{$research->webo}}" type="image/webp"/>
		          <source src="{{$research->image}}" type="{{$research->mime}}"/>
		          <img src="{{$research->image}}" alt="NIVSO - {{$research->name}}" class="w-100" />
		      </picture>
					<p class="px-3 mt-3 mb-2 text-one text-dark"><b>{{$research->name}}</b></p>
					<p class="px-3 text-primary text-one"><u><b>Download</b></u> <i class="fa fa-cloud-download"></i></p>
				</div>
			</a>
		</div>
		@endforeach
	</div>
</div>
@endsection