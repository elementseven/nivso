@php
$page = 'Council Veteran';
$pagetitle = $council->name . " Veterans' Champion - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "Veterans' Champion for " .$council->name;
$pagetype = 'dark';
$pagename = 'veteran-champion';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
  p[data-f-id="pbf"]{
    display: none !important;
  }
</style>
@endsection
@section('header')
<header class="container-fluid position-relative top-padding my-5 text-center text-lg-left mob-px-4">
	<img src="/img/misc/hex.svg" type="image/svg" class="top-right-hex" width="736" height="994" alt="NIVSO hexagon pattern">
	<div class="row">
		<div class="container">
			<div class="row justify-content-center py-5 mob-pb-4">
				<div class="col-md-6 col-lg-5">
					<p class="text-uppercase text-small text-primary letter-spacing mb-2 mt-4 d-lg-none"><b>{{$council->name}}<br/> Veterans' Champion</b></p>
					<h1 class="mb-4 d-lg-none">{{$council->commissioner->full_name}}</h1>
					<picture>
						<source src="{{$council->commissioner->getFirstMediaUrl('commissioners','normal-webp')}}" type="image/webp"/>
						<source src="{{$council->commissioner->getFirstMediaUrl('commissioners','normal')}}" type="{{$council->commissioner->getFirstMedia('commissioners')->mime_type}}"/>
						<img src="{{$council->commissioner->getFirstMediaUrl('commissioners','normal')}}" type="{{$council->commissioner->getFirstMedia('commissioners')->mime_type}}" alt="{{$council->name}} commisioner {{$council->commissioner->full_name}}" width="443" height="569" class="w-100 h-auto mob-mb-4 ipadp-mb-5"/>
					</picture>
				</div>
				<div class="col-lg-7 pl-5 mob-px-3">
					<p class="text-uppercase text-small text-primary letter-spacing mb-2 mt-4 d-none d-lg-block"><b>{{$council->name}} Veteran Champion</b></p>
					<h1 class=" mb-4 d-none d-lg-block">{{$council->commissioner->full_name}}</h1>
					{!! $council->commissioner->description !!}
					<p class="mt-4 mb-2 letter-spacing text-uppercase"><b>Contact {{$council->commissioner->first_name}}</b></p>
					<p class="mb-2"><a href="tel:{{$council->commissioner->phone}}"><i class="fa fa-phone mr-2"></i>{{$council->commissioner->phone}}</a></p>
					<p><a href="mailto:{{$council->commissioner->email}}"><i class="fa fa-envelope-o mr-2"></i>{{$council->commissioner->email}}</a></p>
					@if($council->commissioner->facebook || $council->commissioner->instagram | $council->commissioner->linkedin)
					<p class="mt-4 mb-2 letter-spacing text-uppercase"><b>Follow {{$council->commissioner->first_name}}</b></p>
					<p class="mb-2">
						@if($council->commissioner->facebook)
						<a href="{{$council->commissioner->facebook}}" target="_blank"><i class="fa fa-facebook mr-2"></i></a>
						@endif
						@if($council->commissioner->instagram)
						<a href="{{$council->commissioner->instagram}}" target="_blank"><i class="fa fa-instagram mr-2"></i></a>
						@endif
						@if($council->commissioner->linkedin)
						<a href="{{$council->commissioner->linkedin}}" target="_blank"><i class="fa fa-linkedin mr-2"></i></a>
						@endif
					</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid bg-primary text-white pt-5 need-support-card-container">
	<div class="row">
		<div class="container mb-5 py-4 mob-px-4">
			<div class="row justify-content-center">
				<div class="col-lg-8 mt-3 text-center">
					<h2 class="mb-4">Veteran Support Organisations in {{$council->name}}</h2>
					<p class="px-5">There are a number of organisations throughout {{$council->name}} that provide support to Veterans.</p>
					<a href="/support-organisations">
						<button class="btn btn-white">View Organisations</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container position-relative mob-px-4 text-center text-lg-left">
			<picture>
				<source src="/img/misc/need-support.jpg" type="image/jpeg"/>
				<source src="/img/misc/need-support.webp" type="image/webp"/>
				<img src="/img/misc/need-support.jpg" type="image/jpeg" alt="Need Support? NIVSO" width="720" height="509" class="mw-100 need-support-img z-0" />
			</picture>
			<div class="row position-relative z-2">
				<div class="col-lg-6">
					<div class="card p-5 shadow text-dark mt-5 mob-mt-0 ipadp-mt-0 need-support-card  text-center text-lg-left">
						<p class="mimic-h3">Need help getting support?</p>
						<p>We are the lead support and signposting service linking Veterans and charities supporting Veterans in Northern Ireland.</p>
						<p class="mb-4">If you need more help getting the right support for you, please do not hesitate to get in touch with us.</p>
						<a href="/contact">
							<button type="button" class="btn btn-primary">Get in touch</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@if(count($council->organisations))
<div class="container py-5 my-5 mob-pt-0 ipadp-pt-0 text-center text-lg-left">
	<div class="row justify-content-center py-5 mob-pt-0">
		<div class="col-lg-10 text-center mob-px-4">
			<h2 class="mb-4">Veteran-friendly Organisations in <br class="d-none d-lg-inline-block" />{{$council->name}}</h2>
			<p class="mb-0">We have identified a range of organisations and activities in your area that welcome Veterans to join in and take part in their activities. Some of these organisations have received direct funding to offer Veteran-specific programmes, while others run year-round activities that are also open to the public.</p>
		</div>
	</div>
	<div class="row">
		@foreach($council->organisations as $organisation)
		<div class="col-lg-12 mb-4">
			<div class="card shadow px-3">
				<div class="row">
					<div class="col-lg-4 overflow-hidden bg v-f-organisation-img" style="background-image: url({{$organisation->getFirstMediaUrl('organisations','normal')}});"></div>
					<div class="col-lg-8">
						<div class="py-4 px-4">
							<p class="mimic-h3 mb-0">{{$organisation->name}}</p>
							<hr class="my-3"/>
							
							<p>{!!$organisation->description!!}</p>
							<p class="mb-0"><a href="{{$organisation->website}}" target="_blank" class="mr-3"><i class="fa fa-globe mr-2"></i> Visit Website</a><br class="d-lg-none" /> <a href="tel:{{$organisation->phone}}"><i class="fa fa-phone mr-2"></i> {{$organisation->phone}}</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@else
<div class="py-5"></div>
@endif
@endsection