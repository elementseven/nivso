@php
$page = 'Veteran Champions';
$pagetitle = "Veterans' Champions - Northern Ireland Veterans Support Office | NIVSO";
$metadescription = "A Veterans' Champion is an elected member of the council with an interest in the Armed Forces Community.  There are 11 local councils in Northern Ireland and every one has a Veterans’ Champion.";
$pagetype = 'dark';
$pagename = 'veteran-champions';
$ogimage = 'https://nivso.org.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative top-padding mt-5 mb-4 text-center text-lg-left">
	<div class="row mob-pt-5">
		<div class="col-lg-10">
			<p class="text-uppercase text-small text-primary letter-spacing mb-2 mt-4"><b>Find YOUR Local Veterans' Champion</b></p>
			<h1 class=" mb-3">Select Your Local Council</h1>
			<p>A Veterans' Champion is an elected member of the council with an interest in the Armed Forces Community.  There are 11 local councils in Northern Ireland and every one has a Veterans’ Champion.  You can find contact details for your designated Veterans’ Champion below, along with information on support services in your area.</p> 
			<p>Veterans’ Champions have good knowledge of local services and are a first point of contact for veterans who need information, signposting, or referral to the NIVSO.  Veterans' Champions can also identify and advise local charities or Community Interest Companies who may be eligible to apply for funding from the Armed Forces Covenant Fund Trust.</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5 pb-5 council-areas position-relative z-2">
	<div class="row">
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/armagh-banbridge-and-craigavon.jpg" type="image/jpeg"/>
					<source src="/img/councils/armagh-banbridge-and-craigavon.webp" type="image/webp"/>
					<img src="/img/councils/armagh-banbridge-and-craigavon.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Armagh, Banbridge & Craigavon Council Logo"/>
				</picture>
				<a href="/veteran-champions/armagh-banbridge-and-craigavon">
					<button type="button" class="btn btn-primary overhang-button">Armagh, Banbridge & Craigavon</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/antrim-and-newtownabbey.jpg" type="image/jpeg"/>
					<source src="/img/councils/antrim-and-newtownabbey.webp" type="image/webp"/>
					<img src="/img/councils/antrim-and-newtownabbey.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Antrim & Newtownabbey Council Logo"/>
				</picture>
				<a href="/veteran-champions/antrim-and-newtownabbey">
					<button type="button" class="btn btn-primary overhang-button">Antrim & Newtownabbey</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/mid-and-east-antrim.jpg" type="image/jpeg"/>
					<source src="/img/councils/mid-and-east-antrim.webp" type="image/webp"/>
					<img src="/img/councils/mid-and-east-antrim.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Mid & East Antrim Council Logo"/>
				</picture>
				<a href="/veteran-champions/mid-and-east-antrim">
					<button type="button" class="btn btn-primary overhang-button">Mid & East<br class="d-none d-md-inline-block"/> Antrim</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/ards-and-north-down.jpg" type="image/jpeg"/>
					<source src="/img/councils/ards-and-north-down.webp" type="image/webp"/>
					<img src="/img/councils/ards-and-north-down.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Ards & North Down Council Logo"/>
				</picture>
				<a href="/veteran-champions/ards-and-north-down">
					<button type="button" class="btn btn-primary overhang-button">Ards<br class="d-none d-md-inline-block"/> & North Down</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/belfast-city.jpg" type="image/jpeg"/>
					<source src="/img/councils/belfast-city.webp" type="image/webp"/>
					<img src="/img/councils/belfast-city.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Belfast City Council Logo"/>
				</picture>
				<a href="/veteran-champions/belfast-city">
					<button type="button" class="btn btn-primary overhang-button">Belfast<br class="d-none d-md-inline-block"/> City</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/causeway-coast-and-glens.jpg" type="image/jpeg"/>
					<source src="/img/councils/causeway-coast-and-glens.webp" type="image/webp"/>
					<img src="/img/councils/causeway-coast-and-glens.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Causeway Coast & Glens Council Logo"/>
				</picture>
				<a href="/veteran-champions/causeway-coast-and-glens">
					<button type="button" class="btn btn-primary overhang-button">Causeway Coast & Glens</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/derry-city-and-strabane.jpg" type="image/jpeg"/>
					<source src="/img/councils/derry-city-and-strabane.webp" type="image/webp"/>
					<img src="/img/councils/derry-city-and-strabane.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Derry City & Strabane Council Logo"/>
				</picture>
				<a href="/veteran-champions/derry-city-and-strabane">
					<button type="button" class="btn btn-primary overhang-button">Derry City<br class="d-none d-md-inline-block"/> & Strabane</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/fermanagh-and-omagh.jpg" type="image/jpeg"/>
					<source src="/img/councils/fermanagh-and-omagh.webp" type="image/webp"/>
					<img src="/img/councils/fermanagh-and-omagh.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Fermanagh & Omagh Council Logo"/>
				</picture>
				<a href="/veteran-champions/fermanagh-and-omagh">
					<button type="button" class="btn btn-primary overhang-button">Fermanagh<br class="d-none d-md-inline-block"/> & Omagh</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/newry-mourne-and-down.jpg" type="image/jpeg"/>
					<source src="/img/councils/newry-mourne-and-down.webp" type="image/webp"/>
					<img src="/img/councils/newry-mourne-and-down.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Newry, Mourne & Down Council Logo"/>
				</picture>
				<a href="/veteran-champions/newry-mourne-and-down">
					<button type="button" class="btn btn-primary overhang-button">Newry, Mourne & Down</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/lisburn-and-castlereagh.jpg" type="image/jpeg"/>
					<source src="/img/councils/lisburn-and-castlereagh.webp" type="image/webp"/>
					<img src="/img/councils/lisburn-and-castlereagh.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Lisburn & Castlereagh Council Logo"/>
				</picture>
				<a href="/veteran-champions/lisburn-and-castlereagh">
					<button type="button" class="btn btn-primary overhang-button">Lisburn<br class="d-none d-md-inline-block"/> & Castlereagh</button>
				</a>
			</div>
		</div>
		<div class="col-lg-3 mb-5 mt-3">
			<div class="card border-1 pt-3 px-3 text-center">
				<picture>
					<source src="/img/councils/mid-ulster.jpg" type="image/jpeg"/>
					<source src="/img/councils/mid-ulster.webp" type="image/webp"/>
					<img src="/img/councils/mid-ulster.jpg" width="192" height="100" class="mw-100 w-auto mb-3" alt="Mid Ulster Council Logo"/>
				</picture>
				<a href="/veteran-champions/mid-ulster">
					<button type="button" class="btn btn-primary overhang-button">Mid<br class="d-none d-md-inline-block"/> Ulster</button>
				</a>
			</div>
		</div>
	</div>
</div>
@endsection